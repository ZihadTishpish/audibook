<?php


if (!isset($_SESSION))
{
    session_start();
}

include_once 'Db.php';


if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if ($_POST['query']==='SET_CURRENT_AUDITORIUM')
    {
        $_SESSION['CURRENT_AUDI'] = $_POST['AUDI_ID'];
        echo 'SUCCESS';
    }

    else if ($_POST['query']==='LOAD_AUDI_DATA')
    {
        $id = $_SESSION['CURRENT_AUDI'];
        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();
            //$admin_id = $_SESSION['ADMIN_ID'];
            $result = $dbase->query("SELECT * FROM AUDITORIUM WHERE AUDI_ID='$id' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;
                    echo json_encode($rows);
                }
            }
            else
            {
                echo 'ERROR';
            }
        }
        //echo $id;
    }

    else if ($_POST['query']==='LOAD_AUDI_SECTION_DATA')
    {
        $id = $_SESSION['CURRENT_AUDI'];
        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();
            //$admin_id = $_SESSION['ADMIN_ID'];
            $result = $dbase->query("SELECT * FROM SECTION WHERE AUDI_ID='$id' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
        //echo $id;
    }

    else if ($_POST['query']==='BOOKING_INFO')
    {
        $id = $_SESSION['CURRENT_AUDI'];
        $day = $_POST['DATE'];
        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();

            $result = $dbase->query("SELECT * FROM BOOKING JOIN BOOKING_SLOT WHERE AUDI_ID ='$id' AND BOOKING_DATE='$day' AND BOOKING.BOOKING_ID = BOOKING_SLOT.BOOKING_ID ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
        //echo $id;
    }
    else if ($_POST['query']==='BOOKING_INFO_LIST')
    {
        $id = $_SESSION['CURRENT_AUDI'];
        $day = $_POST['DATE'];
        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else
        {
            $dbase = new Db();
            $result = $dbase->query("SELECT USER.USER_ID, USER_NAME, USER_PHONE, BOOKING_TOTAL_COST, SECTION_ID, SLOT_NUM FROM BOOKING JOIN BOOKING_SLOT JOIN USER WHERE AUDI_ID ='$id' AND BOOKING_DATE='$day' AND BOOKING.BOOKING_ID = BOOKING_SLOT.BOOKING_ID AND BOOKING.USER_ID = USER.USER_ID ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
        //echo $id;
    }

    else if ($_POST['query']==='REQUEST_BOOKING')
    {

        $audi_id = $_SESSION['CURRENT_AUDI'];
        $user_id = $_SESSION['USER_ID'];
        $total = $_POST['TOTAL'];
        $advance = $_POST['ADVANCE'];
        $image = $_POST['IMAGE_SLIP'];
        $date = $_POST['DATE'];
        $status = 'REQUESTED';
        $dbase = new Db();

        $result = $dbase->query2("INSERT INTO BOOKING (USER_ID,
                                AUDI_ID,
                                BOOKING_DATE,
                                BOOKING_TOTAL_COST,
                                BOOKING_TOTAL_ADVANCE,
                                BOOKING_PAYSLIP_LINK,
                                BOOKING_STATUS) VALUES
                                (
                                '".$user_id."', '".$audi_id."','".$date."','".$total."','".$advance."','NULL','REQUESTED'
                                )
                                 ");

        if ($result === 'ERROR')
        {
            echo 'ERROR';

        }
        else
        {
            echo $result;

        }


    }

    else if ($_POST['query']==='REQUEST_BOOKING_SLOT')
    {

        $booking_id = $_POST['BOOKING_ID'];
        $deta = $_POST['DATA'];

        $dbase = new Db();

        $datas = json_decode($deta, true);

        $sql='';
        $time=0;
        foreach ($datas as $data )
        {
            $temp = $data;

            $sql = "INSERT INTO BOOKING_SLOT (BOOKING_ID,
                                SLOT_NUM, SECTION_ID) VALUES
                                (
                                '".$booking_id."', '".$temp['timeslot']."', '".$temp['section']."'
                                );
                                 ";
            $result = $dbase->query($sql);
            echo $result;
            echo $time.'\n';
            $time++;
        }
        echo $sql;

        /*$result = $dbase->mulquery($sql);

        if ($result === 'ERROR')
        {
            echo 'ERROR';

        }
        else
        {
            echo $result;
        }*/


    }

    else if ($_POST['query']==='LOAD_AUDI_SECTION_DATA_ADMIN')
    {
        $id = $_SESSION['AUDITORIUM_ID'];
        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();
            //$admin_id = $_SESSION['ADMIN_ID'];
            $result = $dbase->query("SELECT * FROM SECTION WHERE AUDI_ID='$id' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
        //echo $id;
    }
    else if ($_POST['query']==='BOOKING_INFO_ADMIN')
    {
        $id = $_SESSION['AUDITORIUM_ID'];
        $day = $_POST['DATE'];
        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();

            $result = $dbase->query("SELECT * FROM BOOKING JOIN BOOKING_SLOT WHERE AUDI_ID ='$id' 
            AND BOOKING_DATE='$day' AND BOOKING.BOOKING_ID = BOOKING_SLOT.BOOKING_ID ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
        //echo $id;
    }
    else if ($_POST['query']==='GALLERY_DATA')
    {
        $id = $_SESSION['AUDITORIUM_ID'];

        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();

            $result = $dbase->query("SELECT * FROM GALLERY WHERE AUDI_ID ='$id' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
    }

    else if ($_POST['query']==='GALLERY_DATA_USER')
    {
        $id = $_SESSION['CURRENT_AUDI'];

        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();

            $result = $dbase->query("SELECT * FROM GALLERY WHERE AUDI_ID ='$id' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
    }

    else if ($_POST['query']==='DELETE_FROM_GALLERY')
    {
        $id = $_SESSION['AUDITORIUM_ID'];
        $image_id = $_POST['IMAGE_ID'];

        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();

            $result = $dbase->query("DELETE FROM GALLERY WHERE AUDI_ID ='$id' AND ID='$image_id' ");
            if ($result === true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
            }
        }
    }

    else if ($_POST['query']==='ADD_NEW_SECTION')
    {
        $id = $_SESSION['AUDITORIUM_ID'];
        $name = $_POST['NAME'];
        $capacity = $_POST['CAPACITY'];
        $rent = $_POST['RENT'];

        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();

            $result = $dbase->query2("INSERT INTO SECTION (
                                AUDI_ID,
                                SECTION_NAME,
                                SECTION_RATE,
                                SECTION_CAPACITY
                                ) VALUES
                                (
                                '".$id."', '".$name."','".$rent."','".$capacity."'
                                )
                                 ");
            if ($result === true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
                //echo $result;
            }
        }
    }
    else if ($_POST['query']==='UPDATE_SECTION')
    {
        $id = $_SESSION['AUDITORIUM_ID'];
        $sec_id=$_POST['SECTION_ID'];
        $name = $_POST['NAME'];
        $capacity = $_POST['CAPACITY'];
        $rent = $_POST['RENT'];

        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();
            $sql = "UPDATE SECTION SET SECTION_NAME = '$name', SECTION_CAPACITY='$capacity',SECTION_RATE='$rent'
                  WHERE SECTION_ID= '$sec_id' AND AUDI_ID= '$id' ";
            $result = $dbase->query($sql);

            echo $sql;

            if ($result === true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
                //echo $result;
            }
        }
    }

    else if ($_POST['query']==='UPDATE_AUDI')
    {
        $id = $_SESSION['AUDITORIUM_ID'];

        $name = $_POST['NAME'];
        $capacity = $_POST['ADDRESS'];
        $rent = $_POST['DESC'];

        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();
            $sql = "UPDATE AUDITORIUM SET AUDI_NAME = '$name', AUDI_ADDRESS='$capacity',AUDI_DESCRIPTION='$rent'
                  WHERE  AUDI_ID= '$id' ";
            $result = $dbase->query($sql);

            echo $sql;

            if ($result === true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
                //echo $result;
            }
        }
    }


    else
    {
        echo 'ERROR';
    }
}

else
{
    echo 'ERROR';
}











?>

