<?php
/**
 * Created by PhpStorm.
 * User: tishpish
 * Date: 4/5/17
 * Time: 6:11 PM
 */
//session_start();


if (!isset($_SESSION))
{
    session_start();
}

include_once 'Db.php';

include_once 'sender.php';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{

    if ($_POST['query']==='USER_LOGIN')
    {
        $email =  $_POST['email'];
        $password =  md5($_POST['password']);

        $dbase = new Db();

        if ($_SESSION['LOGIN_TYPE'] ==='USER_LOGIN_PAGE')
        {
            $result = $dbase->query("SELECT * FROM USER WHERE USER_EMAIL='$email' AND USER_PASSWORD='$password' AND USER_VALID = '1' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    echo $user['USER_ID'];
                    $_SESSION['USER_ID']=$user['USER_ID'];
                }
            }
            else
            {
                echo 'ERROR';
            }
        }
        else
        {
            $result = $dbase->query("SELECT * FROM ADMIN WHERE ADMIN_EMAIL='$email' AND ADMIN_PASSWORD='$password' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    echo 'ADMIN_SUCCESS';
                    $_SESSION['ADMIN_ID']=$user['ADMIN_ID'];
                    $_SESSION['AUDITORIUM_ID']=$user['AUDI_ID'];
                }
            }
            else
            {
                echo 'ERROR';
            }
        }


    }

    else if ($_POST['query']==='USER_LOGIN_STATUS')
    {
        if( isset($_SESSION['USER_ID'] ))
        {
            echo 'USER';
        }
        else if (isset($_SESSION['ADMIN_ID']) && isset($_SESSION['AUDITORIUM_ID']))
        {
                echo 'ADMIN';
        }
        else
            echo 'ERROR';
    }

    else if ($_POST['query']==='USER_LOG_OUT')
    {
        $_SESSION['USER_ID'] ='';
        $_SESSION['ADMIN_ID'] ='';
        $_SESSION['AUDITORIUM_ID']='';
        session_destroy();
        echo 'true';
    }

    else if ($_POST['query']==='ADMIN_LOG_OUT')
    {
        $_SESSION['USER_ID'] ='';
        $_SESSION['ADMIN_ID'] ='';
        $_SESSION['AUDITORIUM_ID']='';
        session_destroy();
        echo 'true';
    }

    else if ($_POST['query']==='USER_SIGNUP')
    {
        $email =  $_POST['email'];
        $password =  md5($_POST['password']);
        $name =  $_POST['name'];
        $phone =  $_POST['phone'];
        $address =  $_POST['address'];
        $hash = md5( rand(10000,100000) );

        $dbase = new Db();

         $result = $dbase->query(
            "INSERT INTO USER (USER_EMAIL,
                                USER_NAME,
                                USER_PASSWORD,
                                USER_ADDRESS,
                                USER_PHONE,
                                USER_VALID,
                                USER_PRO_PIC,CHECKING) VALUES
                                (
                                '".$email."', '".$name."','".$password."','".$address."','".$phone."','0','habijabi','".$hash."'
                                )
                                 ");

        if ($result === true)
        {
                //echo 'SUCCESS';
            $send_email = new sender();
            $send_email->sendVerificationBySwift($email,$password,$hash);
        }
        else
        {
            echo 'ERROR';
        }
    }

    else if ($_POST['query']==='USER_LOG_IN_TYPE')
    {
        $_SESSION['LOGIN_TYPE'] ='USER_LOGIN_PAGE';
        echo 'true';
    }

    else if ($_POST['query']==='ADMIN_LOG_IN_TYPE')
    {
        $_SESSION['LOGIN_TYPE'] ='ADMIN_LOGIN_PAGE';
        echo 'true';
    }

    else if ($_POST['query']==='GET_LOG_IN_TYPE')
    {
       // ='ADMIN_LOGIN_PAGE';
        echo  $_SESSION['LOGIN_TYPE'];
    }


    else if ($_POST['query']==='LOAD_USER_PROFILE')
    {
        $user_id = $_SESSION['USER_ID'];

        $dbase = new Db();
        $result = $dbase->query("SELECT * FROM USER WHERE USER_ID='$user_id'  ");
        if ($result->num_rows > 0)
        {
            while ($user = $result->fetch_assoc())
            {
                $rows[] = $user;

            }
            echo json_encode($rows);
        }
        else
        {
            echo 'ERROR';
        }

    }
    else if ($_POST['query']==='LOAD_BOOKING_REQUESTS')
    {

        if (isset($_SESSION['USER_ID']))
        {
            $user_id = $_SESSION['USER_ID'];
            $dbase = new Db();
            $result = $dbase->query("SELECT * FROM BOOKING WHERE USER_ID='$user_id' 
                                    AND BOOKING_STATUS='REQUESTED' ORDER BY BOOKING_DATE DESC");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
        else
        {
            echo 'ERROR';
        }

    }



    else if ($_POST['query']==='LOAD_ALL_REQUESTS')
    {
        if (isset($_SESSION['USER_ID']))
        {
            $user_id = $_SESSION['USER_ID'];

            $dbase = new Db();
            $result = $dbase->query
            (
                "SELECT *
            FROM BOOKING  WHERE
            USER_ID='$user_id'  ORDER BY BOOKING_DATE DESC "
            );

            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }

        }
        else
        {
            echo 'ERROR';
        }

    }
    else if ($_POST['query']==='SELECTED_BOOKING_NUMBER')
    {
        $_SESSION['BOOKING_ID'] =$_POST['BOOKING_ID'];
        echo 'true';
    }

    else if ($_POST['query']==='SELECTED_SLIP_IMAGE')
    {
        if (isset($_SESSION['BOOKING_ID']))
        {
            $booking = $_SESSION['BOOKING_ID'];// =$_POST['BOOKING_ID'];

            $dbase = new Db();
            $result = $dbase->query
            (
                "SELECT BOOKING_PAYSLIP_LINK
            FROM BOOKING  WHERE
            BOOKING_ID='$booking' "
            );

            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;

                }
                echo json_encode($rows);
            }
            else
            {
                echo 'ERROR';
            }
        }
        else
        {
            echo 'no selected booking id in session';
        }


    }
    else if ($_POST['query']==='REMOVE_BOOKING')
    {
        $id = $_SESSION['USER_ID'];
        $booking_id = $_POST['BOOKING_ID'];

        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();
            $result = $dbase->query("DELETE FROM BOOKING WHERE BOOKING_ID ='$booking_id' ");
            if ($result === true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
            }
        }
    }

    else if ($_POST['query']==='UPDATE_USER')
    {

        if( isset($_SESSION['USER_ID']))
        {
            $admin_name = $_POST['name'];
            $admin_phone = $_POST['phone'];
            $admin_post = $_POST['address'];

            $dbase = new Db();
            $admin_id = $_SESSION['USER_ID'];
            $result = $dbase->query("UPDATE USER SET USER_NAME = '$admin_name' , 
                                    USER_ADDRESS='$admin_post',USER_PHONE='$admin_phone' 
                                      WHERE USER_ID='$admin_id' ");

            if ($result=== true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
            }

        }
        else
        {
            echo 'ERROR';
        }
    }

    else if ($_POST['query']==='UPDATE_PASS')
    {

        if( isset($_SESSION['USER_ID']))
        {
            $tish_pass = md5($_POST['PASS']);
            $tish_pass_old = md5($_POST['OLD']);


            $dbase = new Db();
            $admin_id = $_SESSION['USER_ID'];

            $result = $dbase->query("UPDATE USER SET USER_PASSWORD = '$tish_pass'
                                      WHERE USER_ID='$admin_id' AND USER_PASSWORD='$tish_pass_old' ");

            if ($result=== true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
            }

        }
        else
        {
            echo 'ERROR';
        }
    }
    else if ($_POST['query']==='UPDATE_PASS_ADMIN')
    {

        if( isset($_SESSION['ADMIN_ID']))
        {
            $tish_pass = md5($_POST['PASS']);
            $tish_pass_old = md5($_POST['OLD']);


            $dbase = new Db();
            $admin_id = $_SESSION['ADMIN_ID'];

            $result = $dbase->query("UPDATE ADMIN SET ADMIN_PASSWORD = '$tish_pass'
                                      WHERE ADMIN_ID='$admin_id' AND ADMIN_PASSWORD='$tish_pass_old' ");

            if ($result=== true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
            }

        }
        else
        {
            echo 'ERROR';
        }
    }

    else
    {
        echo "ERROR in QUERY";
    }

}


?>
