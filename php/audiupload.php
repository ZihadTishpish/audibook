<?php


if (!isset($_SESSION))
{
    session_start();
}
ini_set('display_errors',1);
error_reporting(E_ALL);

include_once 'Db.php';
$dbase = new Db();

$target_dir = "../uploads/";
///$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$imageName = uniqid().basename($_FILES["fileToUpload"]["name"]);
$target_file = $target_dir .$imageName;

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

if(isset($_POST["submit"]))
{
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false)
    {
        ///echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    }
    else
    {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
    echo "Sorry, only JPG, JPEG & PNG files are allowed.";
    $uploadOk = 0;
}

if (file_exists($target_file))
{
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}

if ($_FILES["fileToUpload"]["size"] > 500000)
{
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0)
{
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
}
else
{
       /// echo 'Directory is:   '.$target_dir;

        $moved = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);

        if ($moved)
        {
            echo "The file "." has been uploaded.";

            if (isset($_SESSION['AUDITORIUM_ID']))
            {
                $id = $_SESSION['AUDITORIUM_ID'];
                $imageLink = "/audibook/uploads/".$imageName;
                $sql = $dbase->query("UPDATE AUDITORIUM SET AUDI_IMAGE = '$imageLink'  
                                      WHERE AUDI_ID='$id' ");
                /*$sql = "INSERT INTO GALLERY (AUDI_ID,LINK) VALUES
                                (
                                '".$id."', '".$imageLink."'
                                );
                                 ";*/

                $result = $dbase->query($sql);
                if ($result === true)
                {
                    echo 'database updated successfully';

                    header('Location: /audibook/dashboard/update');
                    exit;
                }
                else
                {
                    echo 'cannot update database'.$result;
                    header('Location: /audibook/dashboard/update');
                    exit;

                }
            }
            else
            {
                echo "session does not have auditorium id";
            }
        }
        else
        {
            echo "Not uploaded because of error #".$_FILES["file"]["error"];
        }

}
?>