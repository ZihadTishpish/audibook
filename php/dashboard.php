<?php
/**
 * Created by PhpStorm.
 * User: tishpish
 * Date: 4/27/17
 * Time: 2:30 PM
 */


if (!isset($_SESSION))
{
    session_start();
}

include_once 'Db.php';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $admin = $_SESSION['ADMIN_ID'];
    $audi = $_SESSION['AUDITORIUM_ID'];

    if ($_POST['query']==='LOAD_BOOKING_REQUESTS')
    {
        $dbase = new Db();
        $result = $dbase->query("SELECT USER_NAME, USER_PHONE, BOOKING_ID, BOOKING_DATE, BOOKING_TOTAL_COST, BOOKING_PAYSLIP_LINK
                      FROM BOOKING JOIN USER WHERE AUDI_ID='$audi' AND BOOKING_STATUS='REQUESTED' AND BOOKING.USER_ID = USER.USER_ID ORDER BY BOOKING_DATE DESC");
        if ($result->num_rows > 0)
        {
            while ($user = $result->fetch_assoc())
            {
                $rows[] = $user;

            }
            echo json_encode($rows);
        }
        else
        {
            echo 'ERROR';
        }
    }

    else if ($_POST['query']==='LOAD_ALL_REQUESTS')
    {
        $dbase = new Db();
        $result = $dbase->query
        (
            "SELECT 
            USER_NAME, 
            USER_PHONE, 
            BOOKING_ID, 
            BOOKING_DATE, 
            BOOKING_TOTAL_COST, 
            BOOKING_PAYSLIP_LINK, 
            BOOKING_STATUS
            FROM BOOKING JOIN USER WHERE 
            AUDI_ID='$audi' AND 
            BOOKING.USER_ID = USER.USER_ID ORDER BY BOOKING_DATE DESC "
        );

        if ($result->num_rows > 0)
        {
            while ($user = $result->fetch_assoc())
            {
                $rows[] = $user;

            }
            echo json_encode($rows);
        }
        else
        {
            echo 'ERROR';
        }
    }
    else if ($_POST['query']==='LOAD_ADMIN_PROFILE')
    {
        $dbase = new Db();
        $result = $dbase->query
        (
            "SELECT 
            ADMIN_NAME, 
            ADMIN_POST, 
            ADMIN_PHONE
            FROM ADMIN where
            ADMIN_ID='$admin'
            "
        );

        if ($result->num_rows > 0)
        {
            while ($user = $result->fetch_assoc())
            {
                $rows[] = $user;

            }
            echo json_encode($rows);
        }
        else
        {
            echo 'ERROR';
        }
    }
    else if ($_POST['query']==='LOAD_GALLERY')
    {
        $dbase = new Db();
        $result = $dbase->query
        (
            "SELECT * FROM AUDI_IMAGE ADMIN where
            AUDI_ID='$audi'
            "
        );

        if ($result->num_rows > 0)
        {
            while ($user = $result->fetch_assoc())
            {
                $rows[] = $user;

            }
            echo json_encode($rows);
        }
        else
        {
            echo 'ERROR';
        }
    }

    else if ($_POST['query']==='UPDATE_ADMIN')
    {

        if($_SESSION['ADMIN_ID'].length >0 && $_SESSION['AUDITORIUM_ID'].length >0   )
        {
            $admin_name = $_POST['name'];
            $admin_phone = $_POST['phone'];
            $admin_post = $_POST['post'];

            $dbase = new Db();
            $admin_id = $_SESSION['ADMIN_ID'];
            $result = $dbase->query("UPDATE ADMIN SET ADMIN_NAME = '$admin_name' , 
                                    ADMIN_POST='$admin_post',ADMIN_PHONE='$admin_phone' 
                                      WHERE ADMIN_ID='$admin_id' ");

            if ($result=== true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
            }

        }
        else
        {
            echo 'ERROR';
        }
    }

    else if ($_POST['query']==='UPDATE_BOOKING')
    {

        if($_SESSION['ADMIN_ID'].length >0 && $_SESSION['AUDITORIUM_ID'].length >0   )
        {
            $msg_id = $_POST['msg_id'];
            $msg = $_POST['msg'];

            $dbase = new Db();
            $result = $dbase->query("UPDATE BOOKING SET BOOKING_STATUS = '$msg'  
                                      WHERE BOOKING_ID='$msg_id' ");

            if ($result=== true)
            {
                echo 'SUCCESS';
            }
            else
            {
                echo 'ERROR';
            }

        }
        else
        {
            echo 'ERROR';
        }
    }
    else if ($_POST['query']==='LOAD_ADMIN_PROFILE')
    {
        $admin_id = $_SESSION['ADMIN_ID'];

        $dbase = new Db();
        $result = $dbase->query("SELECT * FROM ADMIN WHERE ADMIN_ID='$admin_id'  ");
        if ($result->num_rows > 0)
        {
            while ($user = $result->fetch_assoc())
            {
                $rows[] = $user;

            }
            echo json_encode($rows);
        }
        else
        {
            echo 'ERROR';
        }

    }

    else if ($_POST['query']==='LOAD_AUDI_DATA')
    {
        $id = $_SESSION['AUDITORIUM_ID'];
        if ($id === NULL)
        {
            echo 'ERROR';
        }
        else // there is an auditorium selected
        {
            $dbase = new Db();
            //$admin_id = $_SESSION['ADMIN_ID'];
            $result = $dbase->query("SELECT * FROM AUDITORIUM WHERE AUDI_ID='$id' ");
            if ($result->num_rows > 0)
            {
                while ($user = $result->fetch_assoc())
                {
                    $rows[] = $user;
                    echo json_encode($rows);
                }
            }
            else
            {
                echo 'ERROR';
            }
        }
        //echo $id;
    }

    else
    {
        echo 'Error';
    }

}