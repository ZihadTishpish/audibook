-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 21, 2017 at 04:06 PM
-- Server version: 5.5.54-MariaDB-1ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Audibook`
--
CREATE DATABASE IF NOT EXISTS `Audibook` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `Audibook`;

-- --------------------------------------------------------

--
-- Table structure for table `ADMIN`
--

CREATE TABLE IF NOT EXISTS `ADMIN` (
  `ADMIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUDI_ID` int(11) NOT NULL,
  `ADMIN_NAME` text NOT NULL,
  `ADMIN_POST` text NOT NULL,
  `ADMIN_PRIORITY` int(11) NOT NULL,
  `ADMIN_PRO_PIC` text NOT NULL,
  `ADMIN_PHONE` text NOT NULL,
  `ADMIN_EMAIL` text NOT NULL,
  `ADMIN_PASSWORD` text NOT NULL,
  PRIMARY KEY (`ADMIN_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ADMIN`
--

INSERT INTO `ADMIN` (`ADMIN_ID`, `AUDI_ID`, `ADMIN_NAME`, `ADMIN_POST`, `ADMIN_PRIORITY`, `ADMIN_PRO_PIC`, `ADMIN_PHONE`, `ADMIN_EMAIL`, `ADMIN_PASSWORD`) VALUES
(1, 1, 'Sadikur Rahman', 'Senior Manager', 1, 'localhost/propic/pic1.jpg', '01789458568', 'admin@audibook.com', 'password'),
(2, 2, 'Safiul Haydar', 'Junior Manager', 1, 'http://localhost/propic/pic2.jpg', '01521253167', 'admin2@audibook.com', 'password'),
(3, 3, 'Joynum Abedin', 'Junior Manager', 1, 'http://localhost/propic/pic3.jpg', '01521253167', 'admin3@audibook.com', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `AUDITORIUM`
--

CREATE TABLE IF NOT EXISTS `AUDITORIUM` (
  `AUDI_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUDI_NAME` text NOT NULL,
  `AUDI_ADDRESS` text NOT NULL,
  `AUDI_DESCRIPTION` text NOT NULL,
  `AUDI_IMAGE` text NOT NULL,
  PRIMARY KEY (`AUDI_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `AUDITORIUM`
--

INSERT INTO `AUDITORIUM` (`AUDI_ID`, `AUDI_NAME`, `AUDI_ADDRESS`, `AUDI_DESCRIPTION`, `AUDI_IMAGE`) VALUES
(1, 'Virtual Class Room 1', 'TSC, University of Dhaka', 'The info domain has been the most successful of the seven new domain names', '/images/DU/vc1.jpg'),
(2, 'Virtual Class Room 2', 'Beside VC Office, Dhaka University', 'Virtual class room 2 was established in 2015. This was a courtasy of the government of Bangladesh', '/images/DU/vc1.jpg'),
(3, 'Microbiology Auditorium', 'Mokarrom Bhavan, DU', 'Microbiology auditorium was established in 2015. This was a courtasy of the government of Bangladesh', '/images/DU/micro.jpg'),
(4, 'RC Majumdar Auditorium', 'Beside Arts Faculty, DU', 'RC Majumdar auditorium was established in 2015. This was a courtasy of the government of Bangladesh', '/images/DU/rcmajum.jpg'),
(5, 'TSC auditorium', 'TSC, University of Dhaka', 'TSC is the core part of DU. This auditorium can hold more than 12000 eople with all advanced facilities available in the current world', '/images/DU/TSC2.jpg'),
(6, 'Nawab Ali Chowdhuri Senate Building', 'University of Dhaka', 'This is a description about the auditorium we are talking about. This is a description about the auditorium we are talking about.', '/images/DU/Senate.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `AUDI_IMAGE`
--

CREATE TABLE IF NOT EXISTS `AUDI_IMAGE` (
  `AUDI_IMAGE_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `AUDI_IMAGE_LINK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `BOOKED_SECTION`
--

CREATE TABLE IF NOT EXISTS `BOOKED_SECTION` (
  `BS_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `BOOKING_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `BOOKING`
--

CREATE TABLE IF NOT EXISTS `BOOKING` (
  `BOOKING_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `AUDI_ID` int(11) NOT NULL,
  `BOOKING_DATE` date NOT NULL,
  `BOOKING_TOTAL_COST` int(11) NOT NULL,
  `BOOKING_TOTAL_ADVANCE` int(11) NOT NULL,
  `BOOKING_PAYSLIP_LINK` text NOT NULL,
  `BOOKING_STATUS` text NOT NULL,
  PRIMARY KEY (`BOOKING_ID`),
  KEY `fk_booking_audi_id` (`AUDI_ID`),
  KEY `fk_USER_ID` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `BOOKING`
--

INSERT INTO `BOOKING` (`BOOKING_ID`, `USER_ID`, `AUDI_ID`, `BOOKING_DATE`, `BOOKING_TOTAL_COST`, `BOOKING_TOTAL_ADVANCE`, `BOOKING_PAYSLIP_LINK`, `BOOKING_STATUS`) VALUES
(1, 1, 1, '2017-04-20', 50000, 15000, 'http://localhost/images/habijabi.jpg', 'REQUESTED'),
(2, 2, 1, '2017-04-20', 50000, 15000, 'http://localhost/images/habijabi.jpg', 'TAKEN'),
(28, 1, 1, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED'),
(29, 1, 1, '2017-04-21', 50000, 15000, 'NULL', 'TAKEN'),
(30, 1, 3, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED'),
(31, 1, 3, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED'),
(32, 1, 1, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED'),
(33, 1, 6, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED'),
(34, 1, 6, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED'),
(35, 1, 6, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED'),
(36, 1, 6, '2017-04-21', 50000, 15000, 'NULL', 'REQUESTED');

-- --------------------------------------------------------

--
-- Table structure for table `BOOKING_SLOT`
--

CREATE TABLE IF NOT EXISTS `BOOKING_SLOT` (
  `BOOKING_ID` int(11) NOT NULL DEFAULT '0',
  `SLOT_NUM` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`BOOKING_ID`,`SLOT_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `BOOKING_SLOT`
--

INSERT INTO `BOOKING_SLOT` (`BOOKING_ID`, `SLOT_NUM`) VALUES
(1, 0),
(1, 1),
(1, 2),
(1, 13),
(2, 12),
(28, 0),
(28, 1),
(28, 2),
(28, 3),
(28, 14),
(28, 15),
(28, 16),
(28, 17),
(29, 9),
(30, 0),
(30, 1),
(30, 2),
(30, 3),
(30, 16),
(30, 17),
(30, 18),
(30, 19),
(31, 8),
(32, 6),
(32, 8),
(32, 10),
(32, 12),
(33, 0),
(33, 11),
(33, 22),
(33, 33),
(33, 44),
(33, 55),
(33, 66),
(33, 77),
(33, 88),
(34, 38),
(34, 39),
(34, 49),
(34, 50),
(34, 60),
(34, 61),
(34, 71),
(34, 72),
(34, 82),
(34, 83),
(34, 93),
(34, 94),
(35, 35),
(35, 46),
(35, 57),
(35, 68),
(35, 79),
(36, 25),
(36, 36);

-- --------------------------------------------------------

--
-- Table structure for table `SECTION`
--

CREATE TABLE IF NOT EXISTS `SECTION` (
  `SECTION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUDI_ID` int(11) NOT NULL,
  `SECTION_NAME` text NOT NULL,
  `SECTION_DETAILS` text NOT NULL,
  `SECTION_RATE` text NOT NULL,
  `SECTION_ADVANCE` text NOT NULL,
  PRIMARY KEY (`SECTION_ID`),
  KEY `fk_AUDI_ID` (`AUDI_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `SECTION`
--

INSERT INTO `SECTION` (`SECTION_ID`, `AUDI_ID`, `SECTION_NAME`, `SECTION_DETAILS`, `SECTION_RATE`, `SECTION_ADVANCE`) VALUES
(1, 1, 'Main Room', 'these are the details of the room', '50000', '15000'),
(2, 1, 'Main Room 2', 'these are the details of the room', '50000', '15000'),
(3, 2, 'Hall Room', 'This is a description about the auditorium we are talking about. ', '50000', '15000'),
(4, 2, 'Rest Room', 'This is a description about the auditorium we are talking about. ', '50000', '15000'),
(5, 3, 'Rest Room', 'This is a description about the auditorium we are talking about. ', '50000', '15000'),
(6, 3, 'Main Auditorium', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(7, 4, 'Main Auditorium', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(8, 4, 'Trial Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(9, 5, 'Hall Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(10, 5, 'Hall Room 2', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(11, 6, 'Ground Floor Hall Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(12, 6, 'First Floor Hall Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(13, 6, 'First Floor Lobby', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(14, 6, 'Ground Floor Lobby', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(15, 6, 'Single Bed AC Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(16, 6, 'Single Bed AC Room2', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(17, 6, 'Double Bed AC Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(18, 6, 'Double Bed AC Room2', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(19, 6, 'Double Bed Room2', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(20, 6, 'Double Bed Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000'),
(21, 6, 'Single Non-AC Bed Room', 'This is a description about the auditorium we are talking about. ', '75000', '25000');

-- --------------------------------------------------------

--
-- Table structure for table `USER`
--

CREATE TABLE IF NOT EXISTS `USER` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_EMAIL` text NOT NULL,
  `USER_NAME` text NOT NULL,
  `USER_PASSWORD` text NOT NULL,
  `USER_ADDRESS` text NOT NULL,
  `USER_PHONE` text NOT NULL,
  `USER_VALID` tinyint(1) NOT NULL,
  `USER_PRO_PIC` text NOT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `USER_ID` (`USER_ID`),
  KEY `USER_ID_2` (`USER_ID`),
  KEY `USER_ID_3` (`USER_ID`),
  KEY `USER_VALID` (`USER_VALID`),
  KEY `USER_VALID_2` (`USER_VALID`),
  KEY `USER_VALID_3` (`USER_VALID`),
  KEY `USER_VALID_4` (`USER_VALID`),
  KEY `USER_VALID_5` (`USER_VALID`),
  KEY `USER_VALID_6` (`USER_VALID`),
  KEY `USER_ID_4` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `USER`
--

INSERT INTO `USER` (`USER_ID`, `USER_EMAIL`, `USER_NAME`, `USER_PASSWORD`, `USER_ADDRESS`, `USER_PHONE`, `USER_VALID`, `USER_PRO_PIC`) VALUES
(1, 'alzihadtiash@gmail.com', 'Al Zihad', 'password', 'Dhaka', '01521253162', 1, 'localhost/propic/pic1'),
(2, 'akash@gmail.com', 'Akash', 'password', '29, Pilkhana Road, Dhaka', '01521253166', 0, 'habijabi'),
(109, 'saidul@gamil.com', 'Saidul Hasan', 'password', 'ways floor, Dhaka', '01721290878', 0, 'habijabi');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `BOOKING`
--
ALTER TABLE `BOOKING`
  ADD CONSTRAINT `fk_booking_audi_id` FOREIGN KEY (`AUDI_ID`) REFERENCES `AUDITORIUM` (`AUDI_ID`),
  ADD CONSTRAINT `fk_USER_ID` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`USER_ID`);

--
-- Constraints for table `SECTION`
--
ALTER TABLE `SECTION`
  ADD CONSTRAINT `fk_AUDI_ID` FOREIGN KEY (`AUDI_ID`) REFERENCES `AUDITORIUM` (`AUDI_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
