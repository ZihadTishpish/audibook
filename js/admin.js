/**
 * Created by tishpish on 4/5/17.
 */

    function checker()
    {
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        if (validateEmail(email)&& validatePassword(password))
        {
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function ()
            {
                if (this.readyState == 4 && this.status == 200)
                {
                    if (xhttp.responseText == 'ERROR')
                    {
                        document.getElementById("email_error").innerHTML = 'An error occured';
                    }
                    else
                    {
                        window.location.href = 'dashboard.html';
                    }
                }
            };

            var params = 'query=ADMIN_LOGIN&'+'email='+email+'&password='+password;
            xhttp.open("POST", "/audibook/php/admin_login.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(params);
        }
        else
        {
            if (!validateEmail(email))
            {
                document.getElementById("email_error").innerHTML = "Email not Valid";
            }
            if (!validatePassword(password))
            {

                document.getElementById("password_error").innerHTML = "Password should be between 6 to 16 Characters";
            }
        }

    }

    function info()
    {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (xhttp.responseText == 'ERROR')
                {
                    document.getElementById("password_error").innerHTML = 'An error occured';
                }
                else
                {
                    var response =xhttp.responseText;
                    var data = JSON.parse(response);
                    document.getElementById("password_error").innerHTML = data[0].ADMIN_EMAIL;
                }

            }
        };

        var params = 'query=ADMIN_INFO';
        xhttp.open("POST", "/audibook/php/admin_login.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);
    }

    function validatePassword(password)
    {
        if (password.length>5 && password.length<17)
            return true;
        else
            return false;
    }

    function validateEmail(email)
    {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function load_notifications()
    {
        alert("Hello bro");
    }

    function load_admin_profile()
    {
        xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (xhttp.responseText == 'ERROR')
                {
                    document.getElementById("password_error").innerHTML = 'An error occured';
                }
                else
                {
                    var response =xhttp.responseText;
                    var data = JSON.parse(response);
                    //document.getElementById("ADMIN_NAME").innerHTML = 'Edit Profile of '+data[0].ADMIN_NAME;
                    document.getElementById("name").value = data[0].ADMIN_NAME;
                    document.getElementById("post").value = data[0].ADMIN_POST;
                    document.getElementById("email").value = data[0].ADMIN_EMAIL;
                    document.getElementById("phone").value = data[0].ADMIN_PHONE;
                    document.getElementById("savebutton").innerText = 'SAVE';
                }

            }
        };

        var params = 'query=ADMIN_INFO';
        xhttp.open("POST", "/audibook/php/admin_login.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);
    }

    function update_admin_profile()
    {
        var name = document.getElementById("name").value;
        var post = document.getElementById("post").value;
        var phone = document.getElementById("phone").value;



        if (validate_name(name)==true && validate_name(post)==true && validate_phone(phone)==true)
        {


            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function ()
            {

                if (this.readyState == 4 && this.status == 200)
                {
                    if (xhttp.responseText == 'ERROR')
                    {
                        document.getElementById("savebutton").innerText = 'TRY AGAIN';
                    }
                    else
                    {
                        document.getElementById("savebutton").innerText = 'SAVED';
                    }

                }
            };

            var params = 'query=UPDATE_ADMIN_INFO&ADMIN_NAME='+name+'&ADMIN_PHONE='+phone+'&ADMIN_POST='+post;
            xhttp.open("POST", "/audibook/php/admin_login.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(params);
        }
        else
        {
            if (!validate_name(name))
            {
                document.getElementById("name_error").innerHTML = 'Name not valid';
            }
            else
            {
                document.getElementById("name_error").innerHTML = '';
            }
            if (!validate_name(post))
            {
                document.getElementById("post_error").innerHTML = 'Post not valid';
            }
            else
            {
                document.getElementById("post_error").innerHTML = '';
            }
            if (!validate_phone(phone))
            {
                document.getElementById("phone_error").innerHTML = 'Phone not valid';
            }
            else
            {
                document.getElementById("phone_error").innerHTML = '';
            }
        }


    }



    function load_auditorium_profile()
    {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (xhttp.responseText == 'ERROR')
                {
                    document.getElementById("password_error").innerHTML = 'An error occured';
                }
                else
                {
                    var response =xhttp.responseText;
                    var data = JSON.parse(response);
               //     document.getElementById("password_error").innerHTML = data[0].AUDITORIUM_NAME;
                    document.getElementById("password_error").innerHTML = response;
                }

            }
        };

        var params = 'query=AUDITORIUM_INFO';
        xhttp.open("POST", "/audibook/php/admin_login.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);
    }

    function load_schedule()
    {
        alert("Hello bro");
    }

    function validate_name( x)
    {
        return /^[A-z ]+$/.test(x);
    }
    function validate_phone(x)
    {
        return /^(?:\+88|01)?(?:\d{11}|\d{13})$/.test(x);
    }