/**
 * Created by tishpish on 4/16/17.
 */


totalAmount = 0;
audi_section_data = 0;
section_name=[];
function loadAuditoriumData()
{

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }
    var toda = yyyy+'-'+mm+'-'+dd;

    var box = document.getElementById('date_box');
    if(box != null)
        document.getElementById('date_box').value= toda;


    loadhttp = new XMLHttpRequest();
    loadhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (loadhttp.responseText.trim() == "ERROR".trim())
            {
                //redirect to home page... no auditorium specified
                window.location.href = '/';
            }
            else
            {
                var response =loadhttp.responseText;
                console.log(response);
                try
                {
                    var obj = JSON.parse(response);
                    var length = Object.keys(obj).length;
                    document.getElementById('background_cover').src = obj[0].AUDI_IMAGE;
                    document.getElementById("banner_name_1").innerText = obj[0].AUDI_NAME;
                    document.getElementById("banner_desc_1").innerText = obj[0].AUDI_DESCRIPTION;
                }
                catch (e)
                {
                    console.log(e);
                }

                //loadGalleryData();
                //loadSections();
            }
        }
    };

    var params = 'query=LOAD_AUDI_DATA';
    loadhttp.open("POST", "/audibook/php/audi_tracker.php", true);
    loadhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    loadhttp.send(params);


}

function loadSections()
{
    section_http = new XMLHttpRequest();
    section_http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (section_http.responseText.trim() == "ERROR".trim())
            {
                window.location.href = '/audibook/';
            }
            else
            {
                var response =section_http.responseText;

                try
                {
                    var obj = JSON.parse(response);
                    audi_section_data = obj;

                    var length = Object.keys(obj).length;

                    var sectionTable = document.getElementById('table_data');

                    //sectionTable.innerHTML()
                    var tab_html = '<tr> <th>Time</th>';
                    if (length<4)
                    {
                        tab_html = '<tr> <th style="width: 20%">Time</th>';
                    }


                    for (var i=0;i<length;i++)
                    {
                        tab_html+='<th>'+obj[i].SECTION_NAME+'</th> ';
                        section_name.push(obj[i].SECTION_NAME);
                    }
                    tab_html+='</tr>';

                     times =
                        [
                            '08:00am-09:00am','09:00am-10:00am','10:00am-11:00am','11:00am-12:00pm',
                            '12:00pm-01:00pm','01:00pm-02:00pm','02:00pm-03:00pm','03:00pm-04:00pm',
                            '04:00pm-05:00pm','05:00pm-06:00pm','06:00pm-07:00pm','07:00pm-08:00pm',
                            '08:00pm-09:00pm','09:00pm-10:00pm','10:00pm-11:00pm','11:00pm-12:00pm'
                        ]
                    var count = 0;

                    for (var j = 0; j< 16;j++)
                    {

                        tab_html+='<tr><td>'+times[j]+'</td>';
                        for (var k=0;k<length;k++)
                        {
                            var idk = 'cell'+count;
                            tab_html+='<td id="oka"'+idk+'>'+'<input type="checkbox"  id='+idk+' class="hidden"/>' +
                                '<label for='+idk+' class="cell"></label> </td>';

                            count++;
                        }
                        tab_html+='</tr>';
                        //count++;
                    }
                    sectionTable.innerHTML = tab_html;

                    var table = document.getElementById("table_data");
                    var r_len = table.rows.length;
                    c_len = table.rows[0].cells.length -1;




                    /*document.getElementById("table_data").addEventListener("click",
                        function(e) {
                            if(e.target && e.target.nodeName == "TD")
                            {
                                console.log(e.target);
                            }
                        }
                    );*/

                    loadDaysSchedule();

                    addListeners();
                }
                catch (e)
                {

                }


            }
        }
    };

    var params = 'query=LOAD_AUDI_SECTION_DATA';
    section_http.open("POST", "/audibook/php/audi_tracker.php", true);
    section_http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    section_http.send(params);
}



function loadGalleryData()
{
    var myNode = document.getElementById("gallery_list");
    myNode.innerHTML = '';

    var requestsHttp = new XMLHttpRequest();
    requestsHttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (requestsHttp.responseText == 'ERROR')
            {
                console.log("error");
            }
            else
            {
                var response =requestsHttp.responseText;

                try
                {
                    var obj = JSON.parse(response);
                    var length = Object.keys(obj).length;

                    for (var i=0;i<15;i++)
                    {
                        var div0 = document.createElement("div");
                        div0.id="grid"+i;
                        div0.className="gallery-item gallery-item-"+i;
                        document.getElementById("gallery_list").appendChild(div0);

                        div0.innerHTML=
                            '<a class="example-image-link" style="height: 150px" ' +
                            ' data-lightbox="example-set" style="height: 150px" data-title="">' +
                            '<div class="grid" style="height: 150px"> ' +
                            '<figure class="effect-apollo" style="height: 150px"> ' +
                            '<img style="height: 150px" src="'+obj[i].LINK+'" alt="Asset"/> ' +
                            '</figure> ' +
                            '</div> ' +
                            '</a>';



                    }
                    var div3 = document.createElement("div");
                    div3.className="clearfix";
                    document.getElementById("gallery_list").appendChild(div3);

                    console.log(requestsHttp.responseText);
                }
                catch (e)
                {

                }

            }
        }
    }
    var params = 'query=GALLERY_DATA_USER';
    requestsHttp.open("POST", "/audibook/php/audi_tracker.php", true);
    requestsHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    requestsHttp.send(params);





    
}

function check(data)
{
    alert('hi clicked '+data);
}



function loadDaysSchedule()
{


    var inputs = document.getElementsByTagName("input");
    //var labels = document.getElementsByTagName("input");
    for(var i = 0; i < inputs.length; i++)
    {
        if(inputs[i].type == "checkbox" )
        {
            inputs[i].disabled = false;
        }
    }



    var toda = document.getElementById('date_box').value;


    var slot_data_xhttp = new XMLHttpRequest();
    slot_data_xhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (slot_data_xhttp.responseText === 'ERROR')
            {
                console.log(slot_data_xhttp.responseText);
            }
            else
            {
                var response =slot_data_xhttp.responseText;
                try
                {
                    var data = JSON.parse(response);
                    //console.log(data);
                    var length = Object.keys(data).length;

                    for (var a=0;a<length;a++)
                    {
                        var tileslot = data[a].SLOT_NUM;

                        var section = data[a].SECTION_ID;

                        //console.log(tileslot+'  '+section);

                        var x = getColumnNoOfSection(section);

                        if (x== -1)
                        {
                            console.log("Returned Error");
                        }
                        else
                        {
                          //  console.log("returned :" + x);
                           // console.log('Table to color: collumn=' + x + ' row=' + tileslot + ' status=' + data[a].BOOKING_STATUS);
                            var slot_num = c_len * tileslot + x;
                           // console.log('slot_num: ' + slot_num);




                            var table = document.getElementById("table_data");
                            var r_len = table.rows.length;
                            c_len = table.rows[0].cells.length - 1;



                            var cur_row = Math.floor(slot_num / c_len);
                            var cur_col = slot_num % c_len;


                         //   console.log('for slot ' + slot_num + '  row:' + cur_row + '  col:' + cur_col);

                            var inputs = document.getElementsByTagName("input");

                            var labels = document.getElementsByTagName("label");

                            for (var i = 0; i < inputs.length; i++) {
                                if (inputs[i].type == "checkbox" && inputs[i].id == "cell" + slot_num)
                                {
                                    if (data[a].BOOKING_STATUS == 'REQUESTED') {

                                        labels[i - 1].style.backgroundColor = '#76afed';
                                        inputs[i].disabled = true;
                                        //console.log(data[a].BOOKING_STATUS);
                                    }
                                    else if (data[a].BOOKING_STATUS == 'TAKEN') {
                                        inputs[i].disabled = true;
                                        //console.log(data[a].BOOKING_STATUS);
                                    }

                                }
                            }
                        }
                    }

                }
                catch (e)
                {
                    console.log('No data for this day!'+response);
                }

            }

        }
    };

    var params = 'query=BOOKING_INFO&DATE='+toda+'';
    slot_data_xhttp.open("POST", "/audibook/php/audi_tracker.php", true);
    slot_data_xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    slot_data_xhttp.send(params);
}



function getColumnNoOfSection(data)
{

    var length = Object.keys(audi_section_data).length;
    for (var i=0;i<length;i++)
    {
        var id = audi_section_data[i].SECTION_ID;
        if (id== data)
        {
            return i;
            //console.log("comapring with: "+audi_section_data[i].SECTION_NAME+':  '+audi_section_data[i].SECTION_ID +'=='+ data);
        }
        else
        {
            //console.log("comapring with: "+audi_section_data[i].SECTION_NAME+':  '+audi_section_data[i].SECTION_ID +'!='+ data);
        }
    }
    return -1;
}



function addListeners()
{

    var table = document.getElementById("table_data");
    if (table != null)
    {
        for (var i = 0; i < table.rows.length; i++)
        {
            for (var j = 0; j < table.rows[i].cells.length; j++)
                table.rows[i].cells[j].onclick = function ()
                {
                    tableText(this);
                    console.log('for : '+i+"  "+j);
                };
        }
    }
}



function tableText(tableCell)
{
    //console.log(tableCell.child[0].id);
    console.log('called');
    var c = tableCell.children;

    /// check start
    //console.log(c[0].id);


    var selectedCell = c[0].id.replace('cell','');
    var cur_row = Math.floor(selectedCell/c_len);
    var cur_col = selectedCell%c_len;

    //console.log("selected time: "+times[cur_row]+"  cur_col: "+audi_section_data[cur_col].SECTION_NAME+'   section id: '+audi_section_data[cur_col].SECTION_ID);


    ///////////////////////////////////////////////////////////////////

    if (document.getElementById(c[0].id).checked)
    {
        document.getElementById(c[0].id).checked = false;
        var tk = parseInt(audi_section_data[cur_col].SECTION_RATE);
        calculateTotalCost(tk*-1);

    }
    else
    {
        document.getElementById(c[0].id).checked = true;
        var tk = parseInt(audi_section_data[cur_col].SECTION_RATE);
        calculateTotalCost(tk);
    }

}

function checkAlreadyLoggedIn()
{
    http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'USER')
            {
                document.getElementById('login_prompt').style.display = 'none';
                document.getElementById('rent_option').style.display = 'block';
                loadSections();

            }
            else
            {

                document.getElementById('rent_option').style.display = 'none';
                document.getElementById('login_prompt').style.display = 'block';
            }
        }
    };

    var params = 'query=USER_LOGIN_STATUS';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}


function loadSectionAsList()
{

    var table = document.getElementById("table_section");
    var table2 = document.getElementById("list_section");
    var button = document.getElementById("changeview");

    if (table.style.display == 'none')
    {
        table.style.display = 'block';
        table.style.width = '100%';
        table2.style.display='none';
        button.innerText = 'Show List View'
        loadSections();
    }
    else
    {
        table2.style.display = 'block';
        table.style.display = 'none';
        button.innerText = 'Show Booking View';
        loadList();
    }

}

function loadList()
{
    var toda = document.getElementById('date_box').value;


    var slot_data_xhttp = new XMLHttpRequest();
    slot_data_xhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (slot_data_xhttp.responseText === 'ERROR')
            {
                console.log(slot_data_xhttp.responseText);
            }
            else
            {
                /*var response =slot_data_xhttp.responseText;
                 console.log(response);
                 try
                 {*/
                /*var data = JSON.parse(response);
                 console.log(data);
                 var length = Object.keys(data).length;

                 for (var a=0;a<length;a++)
                 {
                 var tileslot = data[a].SLOT_NUM;

                 var section = data[a].SECTION_ID;

                 */
                /*console.log(tileslot+'  '+section);

                 var x = getColumnNoOfSection(section)

                 if (x== -1)
                 {
                 console.log("Returned Error");
                 }
                 else
                 {
                 //  console.log("returned :" + x);
                 // console.log('Table to color: collumn=' + x + ' row=' + tileslot + ' status=' + data[a].BOOKING_STATUS);
                 var slot_num = c_len * tileslot + x;
                 // console.log('slot_num: ' + slot_num);




                 var table = document.getElementById("table_data");
                 var r_len = table.rows.length;
                 c_len = table.rows[0].cells.length - 1;



                 var cur_row = Math.floor(slot_num / c_len);
                 var cur_col = slot_num % c_len;


                 //   console.log('for slot ' + slot_num + '  row:' + cur_row + '  col:' + cur_col);

                 var inputs = document.getElementsByTagName("input");

                 var labels = document.getElementsByTagName("label");

                 for (var i = 0; i < inputs.length; i++) {
                 if (inputs[i].type == "checkbox" && inputs[i].id == "cell" + slot_num) {
                 if (data[a].BOOKING_STATUS == 'REQUESTED') {

                 labels[i - 1].style.backgroundColor = '#76afed';
                 inputs[i].disabled = true;
                 //console.log(data[a].BOOKING_STATUS);
                 }
                 else if (data[a].BOOKING_STATUS == 'TAKEN') {
                 inputs[i].disabled = true;
                 //console.log(data[a].BOOKING_STATUS);
                 }

                 }
                 }
                 } *//*
             }*/
                /*



                 }
                 catch (e)
                 {
                 console.log('No data for this day!'+response);
                 }*/


                var restofill = document.getElementById('content1');

                var newhtml ='';
                restofill.innerHTML = newhtml;

                var response =slot_data_xhttp.responseText;
                var obj = JSON.parse(response);

                console.log(obj);

                var length = Object.keys(obj).length;

                for (var i=0;i<length;i++)
                {

                    var x = getColumnNoOfSection(obj[i].SECTION_ID);

                    newhtml+= ''+
                        '<tr> ' +
                        '<td>'+obj[i].USER_NAME+'</td> ' +
                        '<td> ' + times[obj[i].SLOT_NUM] +'</td> ' +
                        '<td> ' + section_name[x] +'</td> ' +
                        '<td> ' + '<a href="/audibook/profile?id='+obj[i].USER_ID+'"> Contact</a>' +'</td> ';// +
                }


                restofill.innerHTML = newhtml;

            }

        }
    };

    var params = 'query=BOOKING_INFO_LIST&DATE='+toda+'';
    slot_data_xhttp.open("POST", "/audibook/php/audi_tracker.php", true);
    slot_data_xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    slot_data_xhttp.send(params);
}

document.addEventListener('DOMContentLoaded', function()
{
    totalcost = 0;
    loadAuditoriumData();
    loadGalleryData();
    loadGeneralInfo();
    checkAlreadyLoggedIn();

});

function loadGeneralInfo()
{
    console.log("inside");
    var section_http = new XMLHttpRequest();
    section_http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (section_http.responseText.trim() == "ERROR".trim())
            {
                window.location.href = '/audibook/';
            }
            else
            {
                var response =section_http.responseText;

                try
                {
                    var obj = JSON.parse(response);
                    //audi_section_data = obj;

                    var length = Object.keys(obj).length;
                    var newhtml = '';
                    for (var i=0;i<length;i++)
                    {
                        var name = obj[i].SECTION_NAME;
                        var rate = obj[i].SECTION_RATE;
                        var capacity = obj[i].SECTION_CAPACITY
                        newhtml+='<tr> <td>'+name+'</td> <td>'+capacity+'</td> <td>'+rate+'</td> </tr>'
                    }
                    document.getElementById("content2").innerHTML=newhtml;


                }
                catch (e)
                {

                }


            }
        }
    };

    var params = 'query=LOAD_AUDI_SECTION_DATA';
    section_http.open("POST", "/audibook/php/audi_tracker.php", true);
    section_http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    section_http.send(params);
}

function checkWhatToLoad()
{
    var table = document.getElementById("table_section");
    var table2 = document.getElementById("list_section");
    var button = document.getElementById("changeview");

    if (table.style.display == 'none')
    {
        loadList();
    }
    else
    {
        loadSections();
    }
}

function userLogIn()
{
    logintypehttp = new XMLHttpRequest();
    logintypehttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (logintypehttp.responseText == 'true')
            {
                window.location.href = '/audibook/signin';
            }
        }
    };
    var params = 'query=USER_LOG_IN_TYPE';
    logintypehttp.open("POST", "/audibook/php/user.php", true);
    logintypehttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    logintypehttp.send(params);
}


function bookSlots()
{
    var inputs = document.getElementsByTagName("input");
    var count =0;
    var slots= [];
    var section=[];
    for(var i = 0; i < inputs.length; i++)
    {
        if(inputs[i].type == "checkbox" && inputs[i].disabled != true )
        {
            if (inputs[i].checked)
            {
                var cc = inputs[i].id.replace('cell','');

                var cur_row = Math.floor(cc/c_len);
                var cur_col = cc%c_len;

               // console.log("selected time: "+times[cur_row]+"  cur_col: "+audi_section_data[cur_col].SECTION_NAME);

                slots.push(cur_row);
                section.push(audi_section_data[cur_col].SECTION_ID);
            }
            else
            {
            
            }

        }
    }
   // var allSlots = JSON.stringify(slots);
    //var allSections = JSON.stringify(section);

    var maybad =[];

    if (slots.length >0)
    {
        for (var z=0;z<slots.length;z++)
        {
            var book_data_t = {};
            book_data_t.timeslot = slots[z];
            book_data_t.section = section[z];
            maybad.push(book_data_t);
        }
        var allSlots = JSON.stringify(maybad);

        console.log('total : '+allSlots);


        var booking_date = document.getElementById('date_box').value;
        var booking_total = totalAmount;
        var booking_advance = 15000;
        var slip_image = 'NULL';

        book_slots_http = new XMLHttpRequest();
        book_slots_http.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (book_slots_http.responseText == 'ERROR')
                {
                    console.log(book_slots_http.responseText);
                }
                else
                {
                    var ss = book_slots_http.responseText;
                    var iddd = parseInt(ss);
                    console.log(iddd);

                    slotsavehttp = new XMLHttpRequest();
                    slotsavehttp.onreadystatechange = function ()
                    {
                        if (this.readyState == 4 && this.status == 200)
                        {
                            window.location.href = '/audibook/user';

                            console.log('response: '+ slotsavehttp.responseText);

                        }
                    };
                    var paramss = 'query=REQUEST_BOOKING_SLOT&BOOKING_ID='+iddd+'&DATA='+allSlots+'';
                    slotsavehttp.open("POST", "/audibook/php/audi_tracker.php", true);
                    slotsavehttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    slotsavehttp.send(paramss);

                }
            }
        };
        var params = 'query=REQUEST_BOOKING&DATE='+booking_date+'&TOTAL='+booking_total+
            '&ADVANCE='+booking_advance+'&IMAGE_SLIP='+slip_image+'';
        book_slots_http.open("POST", "/audibook/php/audi_tracker.php", true);
        book_slots_http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        book_slots_http.send(params);
    }



}

function calculateTotalCost(x)
{
    var inputs = document.getElementsByTagName("input");
    var count =0;
    var slots= [];
    var section=[];
    totalAmount = 0;

    for(var i = 0; i < inputs.length; i++)
    {
        if(inputs[i].type == "checkbox" && inputs[i].disabled != true )
        {
            if (inputs[i].checked)
            {
                var cc = inputs[i].id.replace('cell','');

                var cur_row = Math.floor(cc/c_len);
                var cur_col = cc%c_len;

                slots.push(cur_row);
                section.push(audi_section_data[cur_col].SECTION_ID);
                var tk = parseInt(audi_section_data[cur_col].SECTION_RATE);
                totalAmount+=tk;
            }
            else
            {

            }

        }
    }

    console.log(totalAmount +' is the total amount');
    document.getElementById('total_cost').innerText = 'Total: '+totalAmount+' BDT';
}

