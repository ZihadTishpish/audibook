/**
 * Created by tishpish on 4/30/17.
 */


function checkAlreadyLoggedIn()
{
    var tishpishhhtp = new XMLHttpRequest();
    tishpishhhtp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (tishpishhhtp.responseText == 'USER')
            {
               loadUserprofile();
            }
            else if (tishpishhhtp.responseText == 'ADMIN')
            {
                window.location.href = '/audibook/';
            }
            else
            {
                //console.log(tishpishhhtp.responseText);
                window.location.href = '/audibook/';
            }
        }
    };

    var params = 'query=USER_LOGIN_STATUS';
    tishpishhhtp.open("POST", "/audibook/php/user.php", true);
    tishpishhhtp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    tishpishhhtp.send(params);
}

checkAlreadyLoggedIn();

function loadUserprofile()
{
    var  hhtp = new XMLHttpRequest();
    hhtp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            //console.log(hhtp.responseText);
            var response = hhtp.responseText;

            var data = JSON.parse(response);
            //console.log(data);

            var name = data[0].USER_NAME;
            var post = data[0].USER_ADDRESS;
            var phone = data[0].USER_PHONE;

            //console.log(name+'  '+ post+  '   '+  phone)

            document.getElementById('admin_name').innerText= name;
            document.getElementById('admin_post').innerText = post;
            document.getElementById('admin_phone').innerText = phone;

        }
    };

    var params = 'query=LOAD_USER_PROFILE';
    hhtp.open("POST", "/audibook/php/user.php", true);
    hhtp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hhtp.send(params);
}

function userLogout()
{
    var logouthttp = new XMLHttpRequest();
    logouthttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (logouthttp.responseText == 'true')
            {
                window.location.href = '/audibook/';
            }

        }
    };

    var params = 'query=USER_LOG_OUT';
    logouthttp.open("POST", "/audibook/php/user.php", true);
    logouthttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    logouthttp.send(params);
}