/**
 * Created by tishpish on 5/1/17.
 */


function loadBookingRequestes()
{


    var requestsHttp = new XMLHttpRequest();
    requestsHttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (requestsHttp.responseText == 'ERROR')
            {
                var restoill7 = document.getElementById('content1');
                restoill7.innerHTML = '';
                //console.log(requestsHttp.responseText);
            }
            else
            {
                //console.log(requestsHttp.responseText);
                var restofill = document.getElementById('content1');

                var newhtml ='';

                var response =requestsHttp.responseText;
                var obj = JSON.parse(response);

                //console.log(obj);

                var length = Object.keys(obj).length;

                for (var i=0;i<length;i++)
                {
                    var image_link = obj[i].BOOKING_PAYSLIP_LINK;


                    newhtml+= ''+
                        '<tr> ' +
                        '<td>'+obj[i].BOOKING_DATE+'</td> ' +

                        '<td> ' +
                        '<label class="label label-info">'+obj[i].BOOKING_TOTAL_COST+'</label> ' +
                        '</td> ';// +
                    if (image_link == 'NULL')
                    {
                        newhtml +='<td> <button id="noslip'+obj[i].BOOKING_ID+'"  href="#"  class="btn btn-xs btn-danger"  >Upload Slip</button> </td> ' ;
                    }
                    else
                    {
                        newhtml += '<td> <button id="viewslip'+obj[i].BOOKING_ID+'" href="#"  class="btn btn-xs btn-success"  >View Slip</button> </td> ';
                    }
                    newhtml +=
                        '<td> <button id="reject'+obj[i].BOOKING_ID+'" href="#"  class="btn btn-xs btn-danger"  >Cancel Request</button>  </td> </tr>';

                }
                restofill.innerHTML = newhtml;
                addListenerToButton();
            }
        }
    };
    var params = 'query=LOAD_BOOKING_REQUESTS';
    requestsHttp.open("POST", "/audibook/php/user.php", true);
    requestsHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    requestsHttp.send(params);

}




function loadAllRequestes()
{
    //console.log("inside booking request");
    var reqHttp = new XMLHttpRequest();
    reqHttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (reqHttp.responseText == 'ERROR')
            {
                ////console.log(reqHttp.responseText);
                var restoill7 = document.getElementById('content2');
                restoill7.innerHTML = '';
            }
            else
            {
               // //console.log(reqHttp.responseText);
                var restofill = document.getElementById('content2');

                var newhtml ='';

                var response =reqHttp.responseText;
                var obj = JSON.parse(response);

               // //console.log(obj);

                var length = Object.keys(obj).length;

                for (var i=0;i<length;i++)
                {
                    var image_link = obj[i].BOOKING_PAYSLIP_LINK;
                    if (obj[i].BOOKING_STATUS=='TAKEN' || obj[i].BOOKING_STATUS=='REJECTED' )
                    {
                        newhtml+= ''+
                            '<tr> ' +
                            '<td>'+obj[i].BOOKING_DATE+'</td> ' +

                            '<td> ' +
                            '<label class="label label-info">'+obj[i].BOOKING_TOTAL_COST+'</label> ' +
                            '</td> ';// +
                        if (image_link == 'NULL')
                        {
                            newhtml +='<td> <button id="slip'+obj[i].BOOKING_ID+'" href="#"  class="btn btn-xs btn-danger"  >No Slip</button> </td> ' ;
                        }
                        else
                        {
                            newhtml += '<td> <button id="slip'+obj[i].BOOKING_ID+'" href="#"  class="btn btn-xs btn-success"  >View Slip</button> </td> ';
                        }

                        /*if (obj[i].BOOKING_STATUS=='REQUESTED')
                         {
                         newhtml += '<td> <button id="accept'+obj[i].BOOKING_ID+'" href=""  class="btn btn-xs btn-success"  >Accept</button> </td> ' +
                         '<td> <button id="reject'+obj[i].BOOKING_ID+'" href="#"  class="btn btn-xs btn-danger"  >Reject</button>  </td> </tr>';

                         }*/
                        if (obj[i].BOOKING_STATUS=='TAKEN')
                        {
                            newhtml += '<td colspan="2"> <button href=""  class="btn btn-xs btn-success"  >Accepted</button> </td> ';

                        }
                        else if (obj[i].BOOKING_STATUS=='REJECTED')
                        {
                            newhtml += '<td colspan="2"> <button  href=""  class="btn btn-xs btn-danger"  >Rejected</button> </td> ';
                        }
                    }

                }
                restofill.innerHTML = newhtml;

            }
        }
    };
    var params = 'query=LOAD_ALL_REQUESTS';
    reqHttp.open("POST", "/audibook/php/user.php", true);
    reqHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    reqHttp.send(params);

    //console.log("booking request end");
}





function addListenerToButton()
{
    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++)
    {
        var button = buttons[i];
        button.onclick = function(event)
        {
            //alert(event.currentTarget.id);
            var selected_booking_id = event.currentTarget.id;
            if (selected_booking_id.includes('noslip'))
            {

                document.getElementById('audi_cover').src = '/audibook/images/noimage.png';
                console.log("clicked no slip: "+selected_booking_id);
                setSelectedBooking(selected_booking_id.replace('noslip',''),0);
                $('#myModal').modal('show');
            }
            else if (selected_booking_id.includes('viewslip'))
            {
                document.getElementById('audi_cover').src = '/audibook/images/noimage.png';
                console.log("clicked view slip: "+selected_booking_id);
                setSelectedBooking(selected_booking_id.replace('viewslip',''),1);

                $('#myModal').modal('show');
            }
            else if (selected_booking_id.includes('reject'))
            {
                //document.getElementById('audi_cover').src = '/audibook/images/noimage.png';
                console.log("clicked cancellation: "+selected_booking_id);
                cancelRequest(selected_booking_id.replace('reject',''));

            }

        }

    }

}
// this removes a booking request
function cancelRequest(id)
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'SUCCESS')
            {
                console.log('request removed: '+id);
                loadBookingRequestes();
            }
            else
            {
                console.log('delete failed: res = '+http.responseText);

            }
        }
    };

    var params = 'query=REMOVE_BOOKING&BOOKING_ID='+id+'';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}

function getCurrentSlipImage()
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'ERROR')
            {
               console.log('set id success: '+id);
            }
            else
            {
                //console.log('response failed: res = '+http.responseText);
                var obj = JSON.parse(http.responseText);
                document.getElementById('audi_cover').src = obj[0].BOOKING_PAYSLIP_LINK;
            }
        }
    };

    var params = 'query=SELECTED_SLIP_IMAGE';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}

function setSelectedBooking(id, type)
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'true')
            {
                console.log('set id success: '+id);
                if (type==1)
                {
                    getCurrentSlipImage();
                }
            }
            else
            {
                console.log('response failed: res = '+http.responseText);

            }
        }
    };

    var params = 'query=SELECTED_BOOKING_NUMBER&BOOKING_ID='+id+'';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}



function showPickedImage(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#audi_cover')
                .attr('src',e.target.result);

        };
        reader.readAsDataURL(input.files[0]);
    }
}


loadBookingRequestes();
loadAllRequestes();