/**
 * Created by tishpish on 4/27/17.
 */


function loadSections()
{
    section_http = new XMLHttpRequest();
    section_http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (section_http.responseText.trim() == "ERROR".trim())
            {
                window.location.href = '/audibook/';
            }
            else
            {
                var response =section_http.responseText;

                try
                {
                    var obj = JSON.parse(response);
                    audi_section_data = obj;

                    var length = Object.keys(obj).length;

                    var sectionTable = document.getElementById('table_data');




                    //sectionTable.innerHTML()
                    var tab_html = '<tr> <th>Time</th>';
                    if (length<4)
                    {
                        tab_html = '<tr> <th style="width: 15%">Time</th>';
                    }


                    for (var i=0;i<length;i++)
                    {
                        tab_html+='<th>'+obj[i].SECTION_NAME+'</th> ';
                    }
                    tab_html+='</tr>';

                    times =
                        [
                            '08:00am-09:00am','09:00am-10:00am','10:00am-11:00am','11:00am-12:00pm',
                            '12:00pm-01:00pm','01:00pm-02:00pm','02:00pm-03:00pm','03:00pm-04:00pm',
                            '04:00pm-05:00pm','05:00pm-06:00pm','06:00pm-07:00pm','07:00pm-08:00pm',
                            '08:00pm-09:00pm','09:00pm-10:00pm','10:00pm-11:00pm','11:00pm-12:00pm'
                        ]
                    var count = 0;

                    for (var j = 0; j< 16;j++)
                    {

                        tab_html+='<tr><td>'+times[j]+'</td>';
                        for (var k=0;k<length;k++)
                        {
                            var idk = 'cell'+count;
                            tab_html+='<td onclick="" id="oka"'+idk+'>'+'<input type="checkbox"  id='+idk+' class="hidden"/>' +
                                '<label for='+idk+' class="cell"></label> </td>';

                            count++;
                        }
                        tab_html+='</tr>';
                        //count++;
                    }
                    sectionTable.innerHTML = tab_html;

                    var table = document.getElementById("table_data");
                    var r_len = table.rows.length;
                    c_len = table.rows[0].cells.length -1;




                    document.getElementById("table_data").addEventListener("click",
                        function(e) {
                            if(e.target && e.target.nodeName == "TD")
                            {

                            }
                        }
                    );

                    loadDaysSchedule();

                    addListeners();
                }
                catch (e)
                {

                }


            }
        }
    };

    var params = 'query=LOAD_AUDI_SECTION_DATA_ADMIN';
    section_http.open("POST", "/audibook/php/audi_tracker.php", true);
    section_http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    section_http.send(params);
}

function prepareToday()
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }
    var toda = yyyy+'-'+mm+'-'+dd;

    var box = document.getElementById('date_box');
    if(box != null)
        document.getElementById('date_box').value= toda;

    loadSections();
}


function loadDaysSchedule()
{


    var inputs = document.getElementsByTagName("input");
    //var labels = document.getElementsByTagName("input");
    for(var i = 0; i < inputs.length; i++)
    {
        if(inputs[i].type == "checkbox" )
        {
            inputs[i].disabled = false;
        }
    }



    var toda = document.getElementById('date_box').value;


    slot_data_xhttp = new XMLHttpRequest();
    slot_data_xhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (slot_data_xhttp.responseText === 'ERROR')
            {
                console.log(slot_data_xhttp.responseText);
            }
            else
            {
                var response =slot_data_xhttp.responseText;
                try
                {
                    var data = JSON.parse(response);
                    console.log(data);
                    var length = Object.keys(data).length;

                    for (var a=0;a<length;a++)
                    {
                        var tileslot = data[a].SLOT_NUM;

                        var section = data[a].SECTION_ID;

                        console.log(tileslot+'  '+section);

                        var x = getColumnNoOfSection(section)

                        if (x== -1)
                        {
                            console.log("Returned Error");
                        }
                        else
                        {
                            console.log("returned :" + x);
                            console.log('Table to color: collumn=' + x + ' row=' + tileslot + ' status=' + data[a].BOOKING_STATUS);
                            var slot_num = c_len * tileslot + x;
                            console.log('slot_num: ' + slot_num);




                            var table = document.getElementById("table_data");
                            var r_len = table.rows.length;
                            c_len = table.rows[0].cells.length - 1;



                            var cur_row = Math.floor(slot_num / c_len);
                            var cur_col = slot_num % c_len;


                            console.log('for slot ' + slot_num + '  row:' + cur_row + '  col:' + cur_col);

                            var inputs = document.getElementsByTagName("input");

                            var labels = document.getElementsByTagName("label");

                            for (var i = 0; i < inputs.length; i++) {
                                if (inputs[i].type == "checkbox" && inputs[i].id == "cell" + slot_num) {
                                    if (data[a].BOOKING_STATUS == 'REQUESTED') {

                                        labels[i - 1].style.backgroundColor = '#76afed';
                                        inputs[i].disabled = true;
                                        console.log(data[a].BOOKING_STATUS);
                                    }
                                    else if (data[a].BOOKING_STATUS == 'TAKEN') {
                                        inputs[i].disabled = true;
                                        console.log(data[a].BOOKING_STATUS);
                                    }

                                }
                            }
                        }
                    }
                }
                catch (e)
                {
                    console.log('No data for this day!'+response);
                }

            }

        }
    };

    var params = 'query=BOOKING_INFO_ADMIN&DATE='+toda+'';
    slot_data_xhttp.open("POST", "/audibook/php/audi_tracker.php", true);
    slot_data_xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    slot_data_xhttp.send(params);
}


function getColumnNoOfSection(data)
{

    var length = Object.keys(audi_section_data).length;
    console.log(length);
    console.log(audi_section_data);
    for (var i=0;i<length;i++)
    {
        var id = audi_section_data[i].SECTION_ID;
        if (id== data)
        {
            return i;
            //console.log("comapring with: "+audi_section_data[i].SECTION_NAME+':  '+audi_section_data[i].SECTION_ID +'=='+ data);
        }
        else
        {
            //console.log("comapring with: "+audi_section_data[i].SECTION_NAME+':  '+audi_section_data[i].SECTION_ID +'!='+ data);
        }
    }
    return -1;
}

prepareToday();