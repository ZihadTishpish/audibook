/**
 * Created by tishpish on 4/11/17.
 */


function format_page_according_to_login_type()
{
    var getlogintypehttp = new XMLHttpRequest();
    getlogintypehttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (getlogintypehttp.responseText == 'ERROR')
            {
                document.getElementById("email_error").innerHTML = 'An error occured';
            }

            else if (getlogintypehttp.responseText == 'USER_LOGIN_PAGE')
            {
                document.getElementById("login_type").innerHTML = 'Login as User to your account';
            }
            else if (getlogintypehttp.responseText == 'ADMIN_LOGIN_PAGE')
            {
                document.getElementById("login_type").innerHTML = 'Audibook Admin Login';
            }

            else
            {
                alert(getlogintypehttp.responseText);
            }


        }
    };

    var params = 'query=GET_LOG_IN_TYPE';
    getlogintypehttp.open("POST", "/audibook/php/user.php", true);
    getlogintypehttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    getlogintypehttp.send(params);
}


function user_login()
{
    //alert("hello world");
    var email = document.getElementById("email").value;
    var password = document.getElementById("pass").value;
    if (validateEmail(email) && validatePassword(password))
    {
        document.getElementById("pass_error").innerHTML = 'Password';
        document.getElementById("email_error").innerHTML = 'Email';

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                console.log(xhttp.responseText);
                if (xhttp.responseText == 'ERROR')
                {
                    document.getElementById("email_error").innerHTML = 'An error occured';
                }
                else if (xhttp.responseText == 'ADMIN_SUCCESS')
                {
                    window.location.href = '/audibook/dashboard';
                }
                else
                {
                    window.location.href = '/audibook/user';
                }
            }
        };

        var params = 'query=USER_LOGIN&'+'email='+email+'&password='+password;
        xhttp.open("POST", "/audibook/php/user.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);

    }
    else
    {
        if (!validateEmail(email))
        {
            document.getElementById("email_error").innerHTML = 'Email Not Valid';
        }
        else
        {
            document.getElementById("email_error").innerHTML = 'Email';
        }

        if (!validatePassword(password))
        {
            document.getElementById("pass_error").innerHTML = 'Password too short';
        }
        else
        {
            document.getElementById("pass_error").innerHTML = 'Password';
        }
    }
}

function validatePassword(password)
{
    if (password.length>5 && password.length<17)
        return true;
    else
        return false;
}

function validateEmail(email)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate_phone(x)
{
    return /^(?:\+88|01)?(?:\d{11}|\d{13})$/.test(x);
}


function validate_name( x)
{
    return /^[A-z ]+$/.test(x);
}


function user_signup()
{
    window.location.href = '/audibook/signup/';
}



format_page_according_to_login_type();