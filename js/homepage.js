/**
 * Created by tishpish on 4/11/17.
 */

function showalert()
{
    alert("hello");
}

function loadAuditoriumPage(data)
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText.trim() == "SUCCESS".trim())
            {
                //alert("Changing Page");
                window.location.href = '/audibook/auditorium/';
            }

            else
            {
                 //alert(http.responseText+data);
            }
        }
    };

    var params = 'query=SET_CURRENT_AUDITORIUM&AUDI_ID='+data;
    http.open("POST", "/audibook/php/audi_tracker.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}


function checkAlreadyLoggedIn()
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'USER')
            {
                document.getElementById('join_audibook').innerHTML='Log out';
                document.getElementById('join_admin').style.display='none';
                document.getElementById('open_dashboard').style.display='block';

            }
            else if (http.responseText == 'ADMIN')
            {
                document.getElementById('join_audibook').style.display='none';
                document.getElementById('join_admin').innerHTML='Log out';
                document.getElementById('open_dashboard').style.display='block';
            }
            else
            {

                document.getElementById('open_dashboard').style.display='none';
               console.log('not logged in');
            }
        }
    };

    var params = 'query=USER_LOGIN_STATUS';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}

function load_home_page()
{

    checkAlreadyLoggedIn();


    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (xhttp.responseText == 'ERROR')
            {
                console.log(xhttp.responseText);
                //document.getElementById("password_error").innerHTML = 'An error occured';
            }
            else
            {
                var response =xhttp.responseText;

                var obj = JSON.parse(response);
                var length = Object.keys(obj).length;

                for (var i=0;i<length;i++)
                {
                    var div0 = document.createElement("div");
                    div0.id="grid"+obj[i].AUDI_ID;
                    div0.className="col-md-3 popular-grid";
                    div0.style="cursor: pointer";
                    //div0.onclick = 'showNewAlert('+'hello'+')';//('this.id');
                    document.getElementById("audi_container").appendChild(div0);

                    document.getElementById('grid'+obj[i].AUDI_ID).onclick = function ()
                    {
                        //alert('id = '+this.id);
                        var data = this.id.replace('grid','');
                        loadAuditoriumPage(data);
                    }

                    div0.innerHTML ='<img src='+obj[i].AUDI_IMAGE + ' class="img-responsive" alt=""/>';

                    var div = document.createElement("div");
                    div.id="text_content_"+i;
                    div.className="popular-text";
                    document.getElementById("grid"+obj[i].AUDI_ID).appendChild(div);

                    div.innerHTML = '<i class="fa fa-home" aria-hidden="true"></i>';
                    div.innerHTML = '<h5>'+obj[i].AUDI_NAME+'</h5>';

                    var div2 = document.createElement("div");
                    div2.className="detail-bottom";
                    document.getElementById("text_content_"+i).appendChild(div2);

                    div2.innerHTML='<p>'+ obj[i].AUDI_DESCRIPTION +'</p>';

                    if (i%4==3 && i>0)
                    {
                        var div3 = document.createElement("div");
                        div3.className="clearfix";
                        document.getElementById("audi_container").appendChild(div3);
                    }



                }

                var div34 = document.createElement("div");
                div34.className="clearfix";
                document.getElementById("audi_container").appendChild(div34);

            }

        }
    };

    var params = 'query=LOAD_AUDI';
    xhttp.open("POST", "/audibook/php/homepage.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(params);

}

function change_page()
{
    window.location.href = '/audibook/admin';
}

function load_user_login_page()
{
    var title = document.getElementById('join_audibook').innerText;
    if (title == 'Log out')
    {
        //alert('logging out');

        logouthttp = new XMLHttpRequest();
        logouthttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (logouthttp.responseText == 'true')
                {
                    document.getElementById('join_audibook').innerHTML='Join Audibook';
                    document.getElementById('join_admin').style.display='block';
                    document.getElementById('open_dashboard').style.display='none';
                }

            }
        };

        var params = 'query=USER_LOG_OUT';
        logouthttp.open("POST", "/audibook/php/user.php", true);
        logouthttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        logouthttp.send(params);

    }
    else
    {
        logintypehttp = new XMLHttpRequest();
        logintypehttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (logintypehttp.responseText == 'true')
                {
                    window.location.href = '/audibook/signin';
                }
            }
        };
        var params = 'query=USER_LOG_IN_TYPE';
        logintypehttp.open("POST", "/audibook/php/user.php", true);
        logintypehttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        logintypehttp.send(params);

    }
}

function load_admin_login_page()
{
    var title = document.getElementById('join_admin').innerText;
    if (title == 'Log out')
    {

        var logouthttp = new XMLHttpRequest();
        logouthttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (logouthttp.responseText == 'true')
                {
                    document.getElementById('join_admin').innerHTML='Admin';
                    document.getElementById('join_audibook').style.display='block';
                    document.getElementById('open_dashboard').style.display='none';
                }

            }
        };

        var params = 'query=USER_LOG_OUT';
        logouthttp.open("POST", "/audibook/php/user.php", true);
        logouthttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        logouthttp.send(params);

    }
    else
    {
        logintypehttp = new XMLHttpRequest();
        logintypehttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (logintypehttp.responseText == 'true')
                {
                    window.location.href = '/audibook/signin';
                }
            }
        };
        var params = 'query=ADMIN_LOG_IN_TYPE';
        logintypehttp.open("POST", "/audibook/php/user.php", true);
        logintypehttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        logintypehttp.send(params);
    }

}

function load_dashbord()
{
    console.log("dashboard clicked");
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            console.log(http.responseText);
            if (http.responseText == 'USER')
            {
                window.location.href = '/audibook/user';

            }
            else if (http.responseText == 'ADMIN')
            {
                window.location.href = '/audibook/dashboard';
            }
            else
            {
                console.log('not logged in');
            }
        }
    };

    var params = 'query=USER_LOGIN_STATUS';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}
load_home_page();