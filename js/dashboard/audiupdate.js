/**
 * Created by tishpish on 4/29/17.
 */
function loadSections()
{

    var requestsHttp = new XMLHttpRequest();
    requestsHttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (requestsHttp.responseText == 'ERROR')
            {

            }
            else
            {
                var restofill = document.getElementById('section_container');

                var newhtml =
                    '<div class="col-md-3 col-sm-6 col-xs-6" > ' +
                        '<div class="dashboard-div-wrapper bk-clr-one" style="height: 350px; background-color: #45abcb"> ' +
                            '<div class="panel-body"> ' +

                                    '<div class="form-group"> ' +
                                        '<h6 style="float: left">Section Name:</h6> ' +
                                        '<input id="section_name" type="text" class="form-control" id="exampleI" placeholder="Enter Section Name" /> ' +
                                    '</div> ' +
                                    '<div class="form-group"> ' +
                                        '<h6 style="float: left">Capacity:</h6> ' +
                                        '<input id="section_capacity" type="number" class="form-control" id="examp" placeholder="15000" /> ' +
                                    '</div> ' +
                                    '<div class="form-group"> ' +
                                        '<h6 style="float: left">Rent per Hour</h6> ' +
                                        '<input id="section_rent" type="number" class="form-control" id="example" placeholder="25000" /> ' +
                                    '</div> ' +
                                    '<button id="update0" class="btn btn-success">Add New Section</button> ' +

                            '</div> ' +
                        '</div> ' +
                    '</div>';

                var response =requestsHttp.responseText;
                var obj = JSON.parse(response);

                console.log(obj);

                var length = Object.keys(obj).length;

                for (var i=0;i<length;i++)
                {
                   // var image_link = ;
                    newhtml+=
                        '<div class="col-md-3 col-sm-6 col-xs-6" > ' +
                        '<div class="dashboard-div-wrapper bk-clr-one" style="height: 350px"> ' +
                        '<div class="panel-body"> ' +

                        '<div class="form-group"> ' +
                        '<h6 style="float: left">Section Name:</h6> ' +
                        '<input id="section_name'+obj[i].SECTION_ID+'" type="text" class="form-control" id="exampleI" placeholder="Enter Section Name" value="'+obj[i].SECTION_NAME+'"/> ' +
                        '</div> ' +
                        '<div class="form-group"> ' +
                        '<h6 style="float: left">Capacity:</h6> ' +
                        '<input id="section_capacity'+obj[i].SECTION_ID+'" type="number" class="form-control" id="examp" placeholder="15000" value="'+obj[i].SECTION_CAPACITY+'"/> ' +
                        '</div> ' +
                        '<div class="form-group"> ' +
                        '<h6 style="float: left">Rent per Hour</h6> ' +
                        '<input id="section_rent'+obj[i].SECTION_ID+'" type="number" class="form-control" id="example" placeholder="25000" value="'+obj[i].SECTION_RATE+'"/> ' +
                        '</div> ' +
                        '<button id="update'+obj[i].SECTION_ID+'" class="btn btn-success">Update</button> ' +

                        '</div> ' +
                        '</div> ' +
                        '</div>';



                }
                restofill.innerHTML = newhtml;
                addListenerToButton();
            }
        }
    };
    var params = 'query=LOAD_AUDI_SECTION_DATA_ADMIN';
    requestsHttp.open("POST", "/audibook/php/audi_tracker.php", true);
    requestsHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    requestsHttp.send(params);


}


function addListenerToButton()
{
    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++)
    {
        var button = buttons[i];
        button.onclick = function(event)
        {
            //alert(event.currentTarget.id);
            var selected_booking_id = event.currentTarget.id;
            console.log(selected_booking_id);
            var id = selected_booking_id.replace('update','');
            //sendRequest(id);
            showId(id);


        }

    }

}

function showId(x)
{
    console.log('called from update: '+x);
    if (x== '0')
    {
        addNewSection();
    }
    else if (x== '00')
    {
        console.log("auditorium update part");
        audiInfoUpdate();
    }
    else if (x== '000')
    {
        document.getElementById('image_upload_form').style.display= 'block';
        document.getElementById('hahahaha').style.display= 'none';
    }
    else
    {
        //console.log("selected update ")
        updateSection(x);
    }
}

function audiInfoUpdate()
{
    var name = document.getElementById('audi_name').value;
    var address = document.getElementById('audi_address').value;
    var description = document.getElementById('audi_desc').value;

    
    if (name.length > 5 && address.length>7 && description.length<140 && description.length>60)
    {

        console.log(name+'  '+ address+ '         ' + description);
        var audihhtp = new XMLHttpRequest();
        audihhtp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (audihhtp.responseText == 'ERROR')
                {
                    console.log("Could not update auditorium");
                    
                }
                else
                {
                    console.log(audihhtp.responseText);

                    document.getElementById('audi_desc').style.borderColor= '#929292';
                    document.getElementById('audi_name').style.borderColor= '#929292';
                    document.getElementById('audi_address').style.borderColor= '#929292';
                    alert('Update Successful')

                }
            }
        };

        var params = 'query=UPDATE_AUDI&NAME='+name+'&ADDRESS='+address+'&DESC='+description+'';
        audihhtp.open("POST", "/audibook/php/audi_tracker.php", true);
        audihhtp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        audihhtp.send(params);
    }
    else
    {
        if (name.length<=5)
        {
            document.getElementById('audi_name').style.borderColor= '#ff1213';
        }
        else
        {
            document.getElementById('audi_name').style.borderColor= '#929292';
        }

        if (address.length<=7)
        {
            document.getElementById('audi_address').style.borderColor= '#ff1213';
        }
        else
        {
            document.getElementById('audi_address').style.borderColor= '#929292';
        }
        if (description.length<=60 || description.length>=140)
        {
            document.getElementById('audi_desc').style.borderColor= '#ff1213';
        }
        else
        {
            document.getElementById('audi_desc').style.borderColor= '#929292';
        }
    }

}


function updateSection(x)
{
    var sectionid='section'+x;

    var name = document.getElementById('section_name'+x).value;
    var capacity = document.getElementById('section_capacity'+x).value;
    var rent = document.getElementById('section_rent'+x).value;
    console.log(name+'  '+rent+'  '+capacity);


    if (name.length>5 && capacity> 10 && rent > 100)
    {
        var newhttp = new XMLHttpRequest();
        newhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (newhttp.responseText == 'ERROR')
                {
                    console.log("Could not add section");
                    document.getElementById('section_name'+x).style.borderColor='#ff1213';
                    document.getElementById('section_capacity'+x).style.borderColor='#ff1213';
                    document.getElementById('section_rent'+x).style.borderColor='#ff1213';
                }
                else
                {
                    console.log(newhttp.responseText);
                    alert('Update Successful');
                    document.getElementById('section_name'+x).style.borderColor='#929292';
                    document.getElementById('section_capacity'+x).style.borderColor='#929292';
                    document.getElementById('section_rent'+x).style.borderColor='#929292';

                }
            }
        };

        var params = 'query=UPDATE_SECTION&NAME='+name+'&CAPACITY='+capacity+'&RENT='+rent+'&SECTION_ID='+x+'';
        newhttp.open("POST", "/audibook/php/audi_tracker.php", true);
        newhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        newhttp.send(params);
    }
    else
    {
        if (name.length<=5)
        {
            document.getElementById('section_name'+x).style.borderColor='#ff1213';
        }
        else
        {
            document.getElementById('section_name'+x).style.borderColor='#929292';
        }
        if (capacity<=10)
        {
            document.getElementById('section_capacity'+x).style.borderColor='#ff1213';
        }
        else
        {
            document.getElementById('section_capacity'+x).style.borderColor='#929292';
        }
        if (rent<=100)
        {
            document.getElementById('section_rent'+x).style.borderColor='#ff1213';
        }
        else
        {
            document.getElementById('section_rent'+x).style.borderColor='#929292';
        }
    }


}

function addNewSection()
{
    console.log('called from add new');
    var name = document.getElementById('section_name').value;
    var capacity = document.getElementById('section_capacity').value;
    var rent = document.getElementById('section_rent').value;


    if (name.length>5 && capacity> 0 && rent > 0)
    {
        var newhttp = new XMLHttpRequest();
        newhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (newhttp.responseText == 'ERROR')
                {
                    console.log("Could not add section");
                }
                else
                {
                    console.log(newhttp.responseText);
                    loadSections();

                }
            }
        };

        var params = 'query=ADD_NEW_SECTION&NAME='+name+'&CAPACITY='+capacity+'&RENT='+rent+'';
        newhttp.open("POST", "/audibook/php/audi_tracker.php", true);
        newhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        newhttp.send(params);
    }
    else
    {
        console.log("error");
    }

}


function sendRequest(msg_id)
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'ERROR')
            {

            }
            else
            {
                loadImages();

            }
        }
    };

    var params = 'query=DELETE_FROM_GALLERY&IMAGE_ID='+msg_id+'';
    http.open("POST", "/audibook/php/audi_tracker.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}

function loadAudiData()
{
    console.log("inside loading audi data");
    var loadhttp = new XMLHttpRequest();
    loadhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (loadhttp.responseText.trim() == "ERROR".trim())
            {
                window.location.href = '/audibook/dashboard/';
            }
            else
            {
                var response =loadhttp.responseText;
                console.log(response);
                try
                {
                    var obj = JSON.parse(response);
                    var length = Object.keys(obj).length;

                    document.getElementById('audi_name').value = obj[0].AUDI_NAME;
                    document.getElementById('audi_address').value= obj[0].AUDI_ADDRESS;
                    document.getElementById('audi_desc').value= obj[0].AUDI_DESCRIPTION;
                    document.getElementById('audi_cover').src= obj[0].AUDI_IMAGE;

                }
                catch (e)
                {
                    console.log(e);
                }

                //loadGalleryData();
                //loadSections();
            }
        }
    };

    var params = 'query=LOAD_AUDI_DATA';
    loadhttp.open("POST", "/audibook/php/dashboard.php", true);
    loadhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    loadhttp.send(params);
}

loadAudiData();
loadSections();

function showPickedImage(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#audi_cover')
                .attr('src',e.target.result);

        };
        reader.readAsDataURL(input.files[0]);
    }
}