/**
 * Created by tishpish on 4/28/17.
 */
function loadImages()
{

    var requestsHttp = new XMLHttpRequest();
    requestsHttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (requestsHttp.responseText == 'ERROR')
            {

            }
            else
            {
                var restofill = document.getElementById('gallerydata');

                var newhtml ='';

                var response =requestsHttp.responseText;
                var obj = JSON.parse(response);

                console.log(obj);

                var length = Object.keys(obj).length;

                for (var i=0;i<length;i++)
                {
                    var image_link = obj[i].LINK;
                    newhtml+=
                        '<div class="col-lg-6 col-md-6 col-xs-6 thumb"> ' +
                            '<a class="thumbnail" > ' +
                                '<img class="img-responsive" src="'+image_link+'" alt=""  ><br> ' +
                                    '<button id="image'+obj[i].ID+'" class="btn btn-xs btn-danger" style="width: 100%">Delete</button> ' +
                            '</a> ' +
                        '</div>'



                }
                restofill.innerHTML = newhtml;
                addListenerToButton();
            }
        }
    };
    var params = 'query=GALLERY_DATA';
    requestsHttp.open("POST", "/audibook/php/audi_tracker.php", true);
    requestsHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    requestsHttp.send(params);


}


function addListenerToButton()
{
    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++)
    {
        var button = buttons[i];
        button.onclick = function(event)
        {
            //alert(event.currentTarget.id);
            var selected_booking_id = event.currentTarget.id;
            console.log(selected_booking_id);
            var id = selected_booking_id.replace('image','');
            sendRequest(id);



        }

    }

}


function sendRequest(msg_id)
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'ERROR')
            {

            }
            else
            {
                loadImages();

            }
        }
    };

    var params = 'query=DELETE_FROM_GALLERY&IMAGE_ID='+msg_id+'';
    http.open("POST", "/audibook/php/audi_tracker.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}



function showPickedImage(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#audi_cover')
                .attr('src',e.target.result);

        };
        reader.readAsDataURL(input.files[0]);
    }
}


loadImages();