/**
 * Created by tishpish on 4/28/17.
 */
/**
 * Created by tishpish on 4/27/17.
 */


function checkAlreadyLoggedIn()
{
    var tishpishhhtp = new XMLHttpRequest();
    tishpishhhtp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (tishpishhhtp.responseText == 'USER')
            {
                window.location.href = '/audibook/';
            }
            else if (tishpishhhtp.responseText == 'ADMIN')
            {

                loadAllRequestes();
            }
            else
            {
                console.log(tishpishhhtp.responseText);
                window.location.href = '/audibook/';
            }
        }
    };

    var params = 'query=USER_LOGIN_STATUS';
    tishpishhhtp.open("POST", "/audibook/php/user.php", true);
    tishpishhhtp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    tishpishhhtp.send(params);
}




function loadAllRequestes()
{
    var requestsHttp = new XMLHttpRequest();
    requestsHttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (requestsHttp.responseText == 'ERROR')
            {
                var restoill7 = document.getElementById('content1');
                restoill7.innerHTML = '';
            }
            else
            {
                var restofill = document.getElementById('content1');

                var newhtml ='';

                var response =requestsHttp.responseText;
                var obj = JSON.parse(response);

                console.log(obj);

                var length = Object.keys(obj).length;

                for (var i=0;i<length;i++)
                {
                    var image_link = obj[i].BOOKING_PAYSLIP_LINK;
                    if (obj[i].BOOKING_STATUS=='TAKEN' || obj[i].BOOKING_STATUS=='REJECTED' )
                    {
                        newhtml+= ''+
                            '<tr> ' +
                            '<td>'+obj[i].BOOKING_DATE+'</td> ' +
                            '<td>'+obj[i].USER_NAME+'</td> ' +
                            '<td> ' +
                            '<label class="label label-info">'+obj[i].BOOKING_TOTAL_COST+'</label> ' +
                            '</td> ';// +
                        if (image_link == 'NULL')
                        {
                            newhtml +='<td> <button id="noslip"'+' href="#"  class="btn btn-xs btn-danger"  >No Slip</button> </td> ' ;
                        }
                        else
                        {
                            newhtml += '<td> <button id="showslip'+obj[i].BOOKING_ID+'" href="#"  class="btn btn-xs btn-success"  >View Slip</button> </td> ';
                        }

                        /*if (obj[i].BOOKING_STATUS=='REQUESTED')
                         {
                         newhtml += '<td> <button id="accept'+obj[i].BOOKING_ID+'" href=""  class="btn btn-xs btn-success"  >Accept</button> </td> ' +
                         '<td> <button id="reject'+obj[i].BOOKING_ID+'" href="#"  class="btn btn-xs btn-danger"  >Reject</button>  </td> </tr>';

                         }*/
                        if (obj[i].BOOKING_STATUS=='TAKEN')
                        {
                            newhtml += '<td colspan="2"> <button id="accept'+obj[i].BOOKING_ID+'" href=""  class="btn btn-xs btn-success"  >Accepted</button> </td> ';

                        }
                        else if (obj[i].BOOKING_STATUS=='REJECTED')
                        {
                            newhtml += '<td colspan="2"> <button id="accept'+obj[i].BOOKING_ID+'" href=""  class="btn btn-xs btn-danger"  >Rejected</button> </td> ';
                        }
                    }


                    /*newhtml += '' +
                     '<div class="wrapper"> ' +
                     '<div class="box sidebar"> ' +
                     '<img src="'+ image_link+'" style="width: 100%"> ' +
                     '</div> ' +
                     '<div class="box content"> ' +
                     '<p>Requested by: '+obj[i].USER_NAME+' <br>Requested Date: '+obj[i].BOOKING_DATE+' <br> Total Payment: 50000/- </p> ' +
                     '<button id="accept'+obj[i].BOOKING_ID+'" onclick="bookSlots()" class="w3-button w3-green">Accept</button> ' +
                     '<button id="reject'+obj[i].BOOKING_ID+'" onclick="bookSlots()" class="w3-button w3-red">Reject</button> ' +
                     '</div> ' +
                     '</div>'*/
                }
                restofill.innerHTML = newhtml;
                addListenerToButton();
            }
        }
    };
    var params = 'query=LOAD_ALL_REQUESTS';
    requestsHttp.open("POST", "/audibook/php/dashboard.php", true);
    requestsHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    requestsHttp.send(params);
}





function addListenerToButton()
{
    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++)
    {
        var button = buttons[i];
        button.onclick = function(event)
        {
            //alert(event.currentTarget.id);
            var selected_booking_id = event.currentTarget.id;
             if (selected_booking_id.includes('showslip'))
            {
                document.getElementById('audi_cover').src = '/audibook/images/noimage.png';
                console.log("clicked view slip: "+selected_booking_id);
                setSelectedBooking(selected_booking_id.replace('showslip',''),1);

                $('#myModal').modal('show');
            }

            else
            {
                console.log(selected_booking_id);
            }

        }

    }

}





function getCurrentSlipImage()
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'ERROR')
            {
                console.log('set id success: '+id);
            }
            else
            {
                var obj = JSON.parse(http.responseText);
                document.getElementById('audi_cover').src = obj[0].BOOKING_PAYSLIP_LINK;
            }
        }
    };

    var params = 'query=SELECTED_SLIP_IMAGE';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}

function setSelectedBooking(id, type)
{
    var http = new XMLHttpRequest();
    http.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if (http.responseText == 'true')
            {
                console.log('set id success: '+id);
                if (type==1)
                {
                    getCurrentSlipImage();
                }
            }
            else
            {
                console.log('response failed: res = '+http.responseText);

            }
        }
    };

    var params = 'query=SELECTED_BOOKING_NUMBER&BOOKING_ID='+id+'';
    http.open("POST", "/audibook/php/user.php", true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
}






checkAlreadyLoggedIn();