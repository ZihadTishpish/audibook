/**
 * Created by tishpish on 4/18/17.
 */


function validatePassword(password)
{
    if (password.length>5 && password.length<17)
        return true;
    else
        return false;
}

function validateEmail(email)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate_phone(x)
{
    return /^(?:\+88|01)?(?:\d{11}|\d{13})$/.test(x);
}


function validate_name( x)
{
    return /^[A-z ]+$/.test(x);
}


function back_to_homepage()
{
    window.location.href = '/audibook/';
}

function complete_signup()
{

    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var pass = document.getElementById('pass').value;
    var pass_conf = document.getElementById('pass_conf').value;
    var address = document.getElementById('address').value;
    var phone = document.getElementById('phone').value;
    if (validate_name(name) &&
        validateEmail(email) &&
        validatePassword(pass) &&
        address.length> 6 &&
        validate_phone(phone) &&
        (pass == pass_conf))
    {
        document.getElementById('input_form').style.display= 'none';
        document.getElementById('message').innerText='Please Wait';
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function ()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if (xhttp.responseText == 'ERROR')
                {
                    console.log("ERROR");
                    document.getElementById('message').innerText='Signup Failed';
                }
                else
                {
                    console.log(xhttp.responseText);
                    document.getElementById('message').innerText='Verification Email has been sent for authorization. Thank You!';
                }

            }
        };

        var params = 'query=USER_SIGNUP&email='+email+'&password='+pass+'&name='+name+'&address='+address+'&phone='+phone+'';
        xhttp.open("POST", "/audibook/php/user.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);

    }
    else
    {
        if (!validate_name(name))
        {
            document.getElementById('name_error').innerHTML = 'Username not valid';
        }
        else
        {
            document.getElementById('name_error').innerHTML = 'Name';
        }

        if (!validateEmail(email))
        {
            document.getElementById('email_error').innerHTML = 'Email not valid';
        }
        else
        {
            document.getElementById('email_error').innerHTML = 'Email';
        }

        if (!validatePassword(pass))
        {
            document.getElementById('pass_error').innerHTML = 'Password not valid';
        }
        else
        {
            document.getElementById('pass_error').innerHTML = 'Password';
        }

        if (pass != pass_conf)
        {
            document.getElementById('pass_conf_error').innerHTML = 'Password not matched';
        }
        else
        {
            document.getElementById('pass_conf_error').innerHTML = 'Confirm Password';
        }

        if (address.length<7)
        {
            document.getElementById('address_error').innerHTML = 'Address invalid';
        }
        else
        {
            document.getElementById('address_error').innerHTML = 'Address';
        }

        if (!validate_phone(phone))
        {
            document.getElementById('phone_error').innerHTML = 'Phone not valid';
        }
        else
        {
            document.getElementById('phone_error').innerHTML = 'Phone';
        }


    }
}